/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.ekr.measureconverter;

/**
 *
 * @author Marco Scarpa
 */
public class MeasureConverter
{
    public static Double MONITOR_DPI = 72.0;
    public static Double MM_IN_IN = 25.4;
    
		public static Double  pixelToin(Double pixel ,Double dpi) 
		{
			return pixel/dpi;
		}
                
                public static Double  pixelToin(Double pixel) 
		{
			return pixelToin(pixel, MONITOR_DPI);
		}
		
		public static Double  inTopixel(Double inchies ,Double dpi ) 
		{
			return inchies*dpi;
			
		}
                        
                public static Double  inTopixel(Double inchies ) 
		{
			return inTopixel(inchies, MONITOR_DPI);
			
		}
		
		public static Double  pixelTomm(Double pixel ,Double dpi ) //=MONITOR_DPI
		{
			return inTomm(pixelToin(pixel,dpi));
		}
		
                public static Double  pixelTomm(Double pixel ) //=MONITOR_DPI
		{
			return pixelTomm(pixel,MONITOR_DPI);
		}
		
                
		public static Double  mmToPixel(Double millimeters ,Double dpi ) //=MONITOR_DPI
		{
			return inTopixel(mmToin(millimeters),dpi);
		}
                
                public static Double  mmToPixel(Double millimeters ) //=MONITOR_DPI
		{
			return mmToPixel(millimeters,MONITOR_DPI);
		}
                
                 public static Double  inTomm(Double inchies)
		{
			return inchies*MM_IN_IN;
		}
		
		public static Double mmToin (Double millimeters)
		{
			return millimeters/MM_IN_IN;
		}
}
