/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.ekr.pdffromselector.parsingengine.utils;

import java.io.*;
import java.net.*;

/**
 *
 * @author Marco Scarpa
 */
public class FileUtilitiesForPdfFromSelector
{

    /**
     * dice se un file con un determinato URI esiste
     * @param href l'URI del file da verificare
     * @return true se il file esiste
     */
    public static boolean hrefPointedFileExists(URI href)
    {
        return new File(href).exists();
    }
    
}
