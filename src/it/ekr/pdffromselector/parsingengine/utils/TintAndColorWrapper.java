/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.ekr.pdffromselector.parsingengine.utils;

import de.fhcon.idmllib.api.elements.graphic.Color;

/**
 *
 * @author Marco Scarpa
 */
public class TintAndColorWrapper
{

    private Color baseColor = null;

    private Double tintValue = null;

    public TintAndColorWrapper()
    {
        this(null);
    }

    public TintAndColorWrapper(Color baseColor, Double tintValue)
    {
        this.baseColor = baseColor;
        this.tintValue = tintValue;
    }

    public TintAndColorWrapper(
            Color baseColor)
    {
        this.baseColor = baseColor;
        this.tintValue = null;
    }

    /**
     * Get the value of baseColor
     *
     * @return the value of baseColor
     */
    public Color getBaseColor()
    {
        return baseColor;
    }

    /**
     * Set the value of baseColor
     *
     * @param baseColor new value of baseColor
     */
    public void setBaseColor(
            Color baseColor)
    {
        this.baseColor = baseColor;
    }

    /**
     * Get the value of tintValue
     *
     * @return the value of tintValue
     */
    public Double getTintValue()
    {
        return tintValue;
    }

    /**
     * Set the value of tintValue
     *
     * @param tintValue new value of tintValue
     */
    public void setTintValue(Double tintValue)
    {
        this.tintValue = tintValue;
    }

}
