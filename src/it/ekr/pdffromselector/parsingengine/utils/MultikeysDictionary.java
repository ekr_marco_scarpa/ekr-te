/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.ekr.pdffromselector.parsingengine.utils;

import java.util.Dictionary;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;

/**
 *
 * @author Marco Scarpa
 * @param <K>
 * @param <V>
 *
 */
public class MultikeysDictionary<K, V>//K sta per il tipo delle chiavi e v è il tipo dei valori
{

    private Dictionary<K, MultikeysDictionary<K, V>> subdictionaries = null;
    private V currentValue = null;

    public MultikeysDictionary()
    {
    }

    public V get(List<K> keys)
    {
        if (keys.isEmpty())
        {
            return currentValue;
        } else
        {
            K currentLevelKey = keys.get(0);
            List<K> nextLevelKeys = keys.subList(1, keys.size());
            if (subdictionaries != null)
            {
                MultikeysDictionary<K, V> subdictionary = subdictionaries.get(
                        currentLevelKey);
                if (subdictionary != null)
                {
                    return subdictionary.get(nextLevelKeys);
                }
            }

        }

        return null;
    }

    public void put(List<K> keys, V value)
    {
        if (keys.isEmpty())
        {
            currentValue = value;
        } else
        {
            K currentLevelKey = keys.get(0);
            List<K> nextLevelKeys = keys.subList(1, keys.size());
            if (subdictionaries == null)
            {
                subdictionaries = new Hashtable<>();
            }
            MultikeysDictionary<K, V> subdictionary = subdictionaries.get(
                    currentLevelKey);
            if (subdictionary == null)
            {
                subdictionary = new MultikeysDictionary<>();
                subdictionaries.put(currentLevelKey, subdictionary);
            }
            subdictionary.put(nextLevelKeys, value);
        }
    }

    public V remove(List<K> keys)
    {
        if (keys.isEmpty())
        {
            V temp = currentValue;
            currentValue = null;
            return temp;
        } else
        {
            K currentLevelKey = keys.get(0);
            List<K> nextLevelKeys = keys.subList(1, keys.size());
            if (subdictionaries == null)
            {
                return null;
            }
            MultikeysDictionary<K, V> subdictionary = subdictionaries.get(
                    currentLevelKey);
            if (subdictionary == null)
            {
                return null;
            }
            if (subdictionary.isEmpty())
            {
                subdictionaries.remove(currentLevelKey);
                if (subdictionaries.isEmpty())
                {
                    subdictionaries = null;
                }
                return null;
            }
            return subdictionary.remove(nextLevelKeys);
        }
    }

    public boolean isEmpty()
    {
        if (currentValue == null)
        {
            if (subdictionaries == null)
            {
                return true;
            } else
            {
                if (subdictionaries.isEmpty())
                {
                    //non ha senso che tenga un dizionario vuoto
                    subdictionaries = null;
                    return true;
                } else
                {
                    Enumeration<K> keys = subdictionaries.keys();
                    while (keys.hasMoreElements())
                    {
                        K nextElement = keys.nextElement();
                        MultikeysDictionary<K, V> subdic =
                                subdictionaries.get(nextElement);
                        if (subdic.isEmpty() == false)
                        {
                            return false;
                        }

                    }
                    //se sono arrivato qui vuol dire che erano tutti vuoti
                    //noon ha senso che conservi sottodizionari vuoti
                    subdictionaries = null;
                    return true;
                }
            }
        } else
        {
            return false;
        }
    }

    public boolean hasKeys(List<K> keys)
    {
        if (keys.isEmpty())
        {
            if (currentValue != null)
            {
                return true;
            }
        } else
        {
            K currentLevelKey = keys.get(0);
            List<K> nextLevelKeys = keys.subList(1, keys.size());
            if (subdictionaries != null)
            {
                MultikeysDictionary<K, V> subdictionary = subdictionaries.get(
                        currentLevelKey);
                if (subdictionary != null)
                {
                    return subdictionary.hasKeys(nextLevelKeys);
                }
            }

        }

        return false;
    }

}
