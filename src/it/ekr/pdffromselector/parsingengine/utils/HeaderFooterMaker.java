/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.ekr.pdffromselector.parsingengine.utils;

import com.ekrpe.scheduler.model.*;
import com.ekrpe.scheduler.runnable.*;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import de.fhcon.idmllib.api.*;
import it.ekr.coordinatestransformer.*;
import it.ekr.pdffromselector.parsingengine.exceptions.selectordocumentexceptions.*;
import it.ekr.pdffromselector.parsingengine.selectorobjects.SelectorLayoutCell;
import java.io.*;
import java.net.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.*;
import org.xml.sax.*;

/**
 * classe pensata per creare l'intestazione e il piede di pagina
 *
 * @author Marco Scarpa
 */
public class HeaderFooterMaker extends PdfPageEventHelper
{

    private SelectorLayoutCell header = null;
    private SelectorLayoutCell footer = null;
    private PdfPTable headerTable = null;
    private PdfPTable footerTable = null;
    private TaskBase task = null;
    public File file = null;

    @Override
    public void onEndPage(PdfWriter writer, Document document)
    {
       
        try
        {
            double bodyCellWidth = getBodyCell().getWidth();
            Double absoluteXMainCell = getBodyCell().getAbsoluteX();
            
            
            
            if (getHeader() != null)
            {

//            Rectangle artBox = writer.getBoxSize("art");
                getHeaderTable().setTotalWidth((float) bodyCellWidth);
//            System.out.println("artBox.getTop() "+artBox.getTop());
//            System.out.println("artBox.getLeft() "+artBox.getLeft());
                Double absoluteYHeader = getHeader().getAbsoluteY();
                double pdfYheader =
                        CoordinatesTransformer.fromUpperLeftYToLowerLeftY(absoluteYHeader,
                                getPageHeight());
                getHeaderTable().writeSelectedRows(0, -1, absoluteXMainCell.
                       floatValue(), (float) pdfYheader, writer.
                        getDirectContent());
                /*ColumnText headerCt = new ColumnText(writer.getDirectContent());
                Double headerXAbsolute = getHeader().getAbsoluteX();
                Double headerYAbsolute = getHeader().getAbsoluteY();
                double headerXEnding = headerXAbsolute+getHeader().getWidth();
                double pdfYHeader =
                        CoordinatesTransformer.fromUpperLeftYToLowerLeftY(headerYAbsolute, getPageHeight());
                headerCt.setSimpleColumn(absoluteXMainCell.floatValue(),
                        (float) pdfAbsoluteYMainCell,(float) headerXEnding, (float) pdfYHeader);
                headerCt.addElement(getHeaderTable());
                headerCt.go();*/
            }
            if (getFooter() != null)
            {

//            Rectangle artBox = writer.getBoxSize("art");
                getFooterTable().setTotalWidth((float) bodyCellWidth);
                Double absoluteYFooter = getFooter().getAbsoluteY();
                double pdfAbsoluteYfooter =
                        CoordinatesTransformer.fromUpperLeftYToLowerLeftY(absoluteYFooter,
                                getPageHeight());
//
//            
                getFooterTable().writeSelectedRows(0, -1, absoluteXMainCell.floatValue(),(float) pdfAbsoluteYfooter, writer.getDirectContent());
                /*ColumnText footerCt = new ColumnText(writer.getDirectContent());
                double graphicsLly = mainCellYending+footerHeight;
                double pdfLly = CoordinatesTransformer.fromUpperLeftYToLowerLeftY(graphicsLly, getPageHeight());
                Double footerXAbsolute = getFooter().getAbsoluteX();
                double footerXEnding = footerXAbsolute+getFooter().getWidth();
                footerCt.setSimpleColumn(absoluteXMainCell.floatValue(),(float)pdfLly , (float)footerXEnding,(float)mainCellYending);
                footerCt.addElement(getFooterTable());
                footerCt.go();*/
            }
        } catch (SelectorDocumentException |
                ParserConfigurationException | SAXException |
                IdmlLibException | IOException | URISyntaxException ex)
        {
            if (getTask() != null)
            {
                getTask().setStatus(TaskBase.STATUS_ERROR);
                getTask().addLog(new LoggerVO("4",
                        "Error parsing header or footer " + ex.
                        getLocalizedMessage()));
            }
            Logger.getLogger(HeaderFooterMaker.class.getName()).
                    log(Level.SEVERE, null, ex);
        }

    }
    

    /**
     * @return the header
     */
    public SelectorLayoutCell getHeader()
    {
        return header;
    }

    /**
     * @param header the header to set
     */
    public void setHeader(
            SelectorLayoutCell header)
    {
        this.header = header;
    }

    /**
     * @return the footer
     */
    public SelectorLayoutCell getFooter()
    {
        return footer;
    }

    /**
     * @param footer the footer to set
     */
    public void setFooter(
            SelectorLayoutCell footer)
    {
        this.footer = footer;
    }

    /**
     * @return the headerTable
     */
    public PdfPTable getHeaderTable()
    {
        return headerTable;
    }

    /**
     * @param headerTable the headerTable to set
     */
    public void setHeaderTable(PdfPTable headerTable)
    {
        this.headerTable = headerTable;
    }

    /**
     * @return the footerTable
     */
    public PdfPTable getFooterTable()
    {
        return footerTable;
    }

    /**
     * @param footerTable the footerTable to set
     */
    public void setFooterTable(PdfPTable footerTable)
    {
        this.footerTable = footerTable;
    }

    /**
     * @return the task
     */
    public TaskBase getTask()
    {
        return task;
    }

    /**
     * @param task the task to set
     */
    public void setTask(TaskBase task)
    {
        this.task = task;
    }

    private SelectorLayoutCell bodyCell = null;
    private double pageHeight = 0;

    /**
     * @return the mainCell
     */
    public SelectorLayoutCell getBodyCell()
    {
        return bodyCell;
    }

    /**
     * @param bodyCell the mainCell to set
     */
    public void setBodyCell(
            SelectorLayoutCell bodyCell)
    {
        this.bodyCell = bodyCell;
    }

    /**
     * @return the pageHeight
     */
    public double getPageHeight()
    {
        return pageHeight;
    }

    /**
     * @param pageHeight the pageHeight to set
     */
    public void setPageHeight(double pageHeight)
    {
        this.pageHeight = pageHeight;
    }
}
