/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.ekr.pdffromselector.parsingengine.utils;

import com.itextpdf.text.*;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.pdf.*;
import it.ekr.measureconverter.*;

/**
 *
 * @author Marco Scarpa
 */
public class PageNumberCreator extends PdfPageEventHelper
{

    public Double xOffset = null;//in mm
    public Double yOffset = null;//in mm
    public Double fontSize = null;//in punti
    @Override
    public void onEndPage(PdfWriter writer, Document document)
    {
        if (xOffset != null)
        {
            
            if (yOffset != null)
            {
                Double xOffsetPixel = MeasureConverter.mmToPixel(xOffset);
                Double yOffsetPixel = MeasureConverter.mmToPixel(yOffset);
                int pageNumber = document.getPageNumber();
                if (fontSize == null)
                {
                    fontSize = DEFAULT_FONT_SIZE_FOR_PAGE_NUMBER;
                }
                ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_BASELINE, new Phrase(""+pageNumber,new Font(FontFamily.HELVETICA, fontSize.floatValue())), xOffsetPixel.floatValue(), yOffsetPixel.floatValue(), 0);
            }
        }
    }
    public static final double DEFAULT_FONT_SIZE_FOR_PAGE_NUMBER = 9.0;
    
}
