/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.ekr.pdffromselector.parsingengine.utils;

import it.ekr.pdffromselector.parsingengine.selectorobjects.*;
import java.util.*;

/**
 *
 * @author Marco Scarpa
 */
public class CompareSelectorLayoutCellsByFlowIndex implements Comparator<SelectorLayoutCell>
{

    @Override
    public int compare(SelectorLayoutCell o1, SelectorLayoutCell o2)
    {
        //negativo se o1 precede o2
        Integer flowIndex1 = o1.getFlowIndex();
        Integer flowIndex2 = o2.getFlowIndex();
        if (flowIndex1 != null)
        {
            if (flowIndex2 == null)
            {
                //quelle senza indice di flusso vanno per ultime, quindi o1 precede o2
                return -1;
            }else
            {
                return flowIndex1-flowIndex2;
            }
        }else
        {
            if (flowIndex2 == null)
            {
                return 0;
            }else
            {
                //quelle senza indice di flusso vanno per ultime, quindi o1 segue o2
                return 1;
            }
        }
       
    }
    
}
