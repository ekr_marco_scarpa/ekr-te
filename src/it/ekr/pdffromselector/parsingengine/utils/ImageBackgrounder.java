/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.ekr.pdffromselector.parsingengine.utils;

import com.ekrpe.scheduler.model.*;
import com.ekrpe.scheduler.runnable.*;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;

import it.ekr.pdffromselector.parsingengine.exceptions.pdfexceptions.ImageBackgroundException;

import java.io.*;
import java.util.logging.*;

/**
 *
 * @author Marco Scarpa
 */
public class ImageBackgrounder extends PdfPageEventHelper
{
    protected PdfImportedPage backgroundPdfPage = null;
    protected Image backgroundImage = null;
    private TaskBase task = null;
    private File file = null;

    public void setBackgroundImage(File imageFile,PdfWriter writer) throws ImageBackgroundException
    {
        try{
        		 //resetto quanto memorizzato per usare i nuovi contenuti
                resetStoredImage();
                if (imageFile != null)
                {
                    String canonicalPath = imageFile.getCanonicalPath();
                    String toLowerCase = canonicalPath.toLowerCase();
                    if (toLowerCase.endsWith(".pdf"))
                    {
                        //se è un pdf devo estrarre la prima pagina
                        PdfReader reader = new PdfReader(canonicalPath);
                        backgroundPdfPage = writer.getImportedPage(reader, 1);
                    }else
                    {
                        backgroundImage = Image.getInstance(canonicalPath);
                    }
                    
                    
                }
        	}catch (Exception ex)
			{
			   throw new ImageBackgroundException(ex);
			}
       
        
    }
    
    
    public void resetStoredImage()
    {
        backgroundPdfPage = null;
        backgroundImage = null;
    }
    
    @Override
    public void onEndPage(PdfWriter writer, Document document)
    {
      
        PdfContentByte directContentUnder = writer.getDirectContentUnder();
        if (backgroundPdfPage != null)
        {
            directContentUnder.addTemplate(backgroundPdfPage, 0,0);
        }else if (backgroundImage != null)
        {
            try {
                backgroundImage.setAbsolutePosition(0, 0);
                directContentUnder.addImage(backgroundImage);
            } catch (DocumentException ex) {
                if (getTask() !=null)
                {
                    getTask().setStatus(TaskBase.STATUS_ERROR);
                    getTask().addLog(new LoggerVO("5", "Error putting background image "+ex.getLocalizedMessage()));
                }
                Logger.getLogger(ImageBackgrounder.class.getName()).
                        log(Level.SEVERE, null, ex);
            }
        }
       
    }

    /**
     * @return the task
     */
    public TaskBase getTask()
    {
        return task;
    }

    /**
     * @param task the task to set
     */
    public void setTask(TaskBase task)
    {
        this.task = task;
    }

    /**
     * @return the file
     */
    public File getFile()
    {
        return file;
    }

    /**
     * @param file the file to set
     */
    public void setFile(File file)
    {
        this.file = file;
    }
    
}
