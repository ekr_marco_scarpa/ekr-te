/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.ekr.pdffromselector.parsingengine.utils;

import com.itextpdf.text.*;
import de.fhcon.idmllib.api.elements.Idml;
import it.ekr.pdffromselector.parsingengine.selectorobjects.SelectorDocument;

/**
 *
 * @author Marco Scarpa
 */
public class PdfWrapperException extends Exception
{
    /**
	 * 
	 */
	private static final long serialVersionUID = -3600144420559602989L;
	private Document pdf = null;
        private SelectorDocument selectorDoc = null;
        private Idml idmlDoc = null ;
    private Exception exception = null;

    /**
     * @return the pdf
     */
    public Document getPdf()
    {
        return pdf;
    }

    /**
     * @param pdf the pdf to set
     */
    public void setPdf(Document pdf)
    {
        this.pdf = pdf;
    }

    /**
     * @return the exception
     */
    public Exception getException()
    {
        return exception;
    }

    /**
     * @param exception the exception to set
     */
    public void setException(Exception exception)
    {
        this.exception = exception;
    }

    /**
     * @return the selectorDoc
     */
    public SelectorDocument getSelectorDoc()
    {
        return selectorDoc;
    }

    /**
     * @param selectorDoc the selectorDoc to set
     */
    public void setSelectorDoc(SelectorDocument selectorDoc)
    {
        this.selectorDoc = selectorDoc;
    }

    /**
     * @return the idmlDoc
     */
    public Idml getIdmlDoc()
    {
        return idmlDoc;
    }

    /**
     * @param idmlDoc the idmlDoc to set
     */
    public void setIdmlDoc(Idml idmlDoc)
    {
        this.idmlDoc = idmlDoc;
    }
}
