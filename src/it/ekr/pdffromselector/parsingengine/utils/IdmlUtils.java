package it.ekr.pdffromselector.parsingengine.utils;

import de.fhcon.idmllib.api.IdmlLibException;
import de.fhcon.idmllib.api.elements.Document;
import de.fhcon.idmllib.api.elements.complexproperties.BasedOn;
import de.fhcon.idmllib.api.elements.graphic.Color;
import de.fhcon.idmllib.api.elements.graphic.Graphic;
import de.fhcon.idmllib.api.elements.graphic.Tint;
import de.fhcon.idmllib.api.elements.styles.CellStyle;
import de.fhcon.idmllib.api.elements.styles.CellStyleGroup;
import de.fhcon.idmllib.api.elements.styles.ObjectStyle;
import de.fhcon.idmllib.api.elements.styles.ObjectStyleGroup;
import de.fhcon.idmllib.api.elements.styles.ParagraphStyle;
import de.fhcon.idmllib.api.elements.styles.ParagraphStyleGroup;
import de.fhcon.idmllib.api.elements.styles.RootCellStyleGroup;
import de.fhcon.idmllib.api.elements.styles.RootObjectStyleGroup;
import de.fhcon.idmllib.api.elements.styles.RootParagraphStyleGroup;
import de.fhcon.idmllib.api.elements.styles.RootTableStyleGroup;
import de.fhcon.idmllib.api.elements.styles.Styles;
import de.fhcon.idmllib.api.elements.styles.TableStyle;
import de.fhcon.idmllib.api.elements.styles.TableStyleGroup;
import java.util.ArrayList;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author Marco Scarpa
 */
public class IdmlUtils
{

    protected static Dictionary<Document, Dictionary<String, ParagraphStyle>> paragraphStyleCacheBySelf =
            null;
    protected static Dictionary<Document, Dictionary<String, ParagraphStyle>> paragraphStyleCacheByName =
            null;
    protected static Dictionary<Document, Dictionary<String, CellStyle>> cellStyleCacheBySelf =
            null;
    protected static Dictionary<Document, Dictionary<String, CellStyle>> cellStyleCacheByName =
            null;
    protected static Dictionary<Document, Dictionary<String, TableStyle>> tableStyleCacheBySelf =
            null;
    protected static Dictionary<Document, Dictionary<String, TableStyle>> tableStyleCacheByName =
            null;
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    protected static Dictionary<Document, Dictionary<String, ObjectStyle>> objectStyleCacheBySelf =
            null;
    protected static Dictionary<Document, Dictionary<String, ObjectStyle>> objectStyleCacheByName =
            null;
    public static final String GROUP_TRAILER = "_gruppo:";
    public static final String PARAGRAPH_STYLE_PREFIX = "ParagraphStyle/";
    public static final String CELL_STYLE_PREFIX = "CellStyle/";
    public static final String TABLE_STYLE_PREFIX = "TableStyle/";
    public static final String OBJECT_STYLE_PREFIX = "ObjectStyle/";
    
    protected static Dictionary<Document, Dictionary<String, Color>> colorsCacheBySelf =
            null;
    protected static Dictionary<Document, Dictionary<String, Tint>> tintsCacheBySelf =
            null;
    public static final String SWATCH_NONE = "Swatch/None";
    public static final String COLOR_PREFIX = "Color/";
    public static final String TINT_PREFIX = "Tint/";

    /**
     *
     * @param geometricBounds
     * @return
     */
    public static Double calculateHeightFromGeometricBounds(
            String geometricBounds)
    {
        String[] parsed = geometricBounds.split(" ");
        if (parsed.length == 4)
        {
            int startingIndex = 0;
            int endingIndex = 2;
            return lengthCalculationFromBounds(parsed, startingIndex,
                    endingIndex);
        }

        return null;
    }

    private static Double lengthCalculationFromBounds(
            String[] parsedGeometricBounds, int startingIndex, int endingIndex)
            throws NumberFormatException
    {
        String startingHeight = parsedGeometricBounds[startingIndex];
        String endingHeight = parsedGeometricBounds[endingIndex];
        Double startingHeightDouble = new Double(startingHeight);
        Double endingHeightDouble = new Double(endingHeight);
        Double heightDouble = endingHeightDouble - startingHeightDouble;
        return heightDouble;
    }

    /**
     *
     * @param geometricBounds
     * @return
     */
    public static Double calculateWidthFromGeometricBounds(
            String geometricBounds)
    {
        String[] parsed = geometricBounds.split(" ");
        if (parsed.length == 4)
        {
            int startingIndex = 1;
            int endingIndex = 3;
            return lengthCalculationFromBounds(parsed, startingIndex,
                    endingIndex);
        }
        return null;
    }

    /**
     *
     * @param geometricBounds
     * @return
     */
    public static Dimensions calculateHeightAndWidthFromGeometricBounds(
            String geometricBounds)
    {
        Double height = calculateHeightFromGeometricBounds(geometricBounds);
        Double width = calculateWidthFromGeometricBounds(geometricBounds);
        return new Dimensions(height, width);
    }

    public static String convertTypePathToIdmlName(List<String> typePath)
    {
        Iterator<String> iterator = typePath.iterator();
        String buildingName = "";
        boolean isLastItem = false;
        while (iterator.hasNext())
        {
            String extractedType = iterator.next();
            if (iterator.hasNext() == false)
            {
                isLastItem = true;
            }
            if (!isLastItem)
            {
                extractedType += GROUP_TRAILER;
            }
            buildingName += extractedType;
        }
        return buildingName;
    }

    /**
     * trova lo stile di paragrafo usando il self dello stile
     *
     * @param supertemplateDocument il supertemplate su cui si cerca lo stile di
     * paragrafo
     * @param self il self dello stile di paragrafo cercato
     * @return lo stile di paragrafo con il self indicato o null se non viene
     * trovato
     * @throws IdmlLibException
     */
    public static ParagraphStyle retrieveParagraphStyleBySelf(
            Document supertemplateDocument, String self) throws IdmlLibException
    {

        ParagraphStyle cached = retrieveCachedParagraphStyleBySelf(
                supertemplateDocument, self);
        if (cached != null)
        {
            return cached;
        }
        return retrieveParagraphStyleBySelfOrName(supertemplateDocument, self,
                true);
    }

    private static ParagraphStyle retrieveCachedParagraphStyleBySelf(
            Document supertemplateDocument, String self)
    {
        if (paragraphStyleCacheBySelf != null)
        {
            Dictionary<String, ParagraphStyle> cachedForDoc =
                    paragraphStyleCacheBySelf.get(supertemplateDocument);
            if (cachedForDoc != null)
            {
                ParagraphStyle cached = cachedForDoc.get(self);
                if (cached != null)
                {
                    return cached;
                }
            }
        }
        return null;
    }

    private static ParagraphStyle retrieveCachedParagraphStyleByName(
            Document supertemplateDocument, String name)
    {
        if (paragraphStyleCacheByName != null)
        {
            Dictionary<String, ParagraphStyle> cachedForDoc =
                    paragraphStyleCacheByName.get(supertemplateDocument);
            if (cachedForDoc != null)
            {
                ParagraphStyle cached = cachedForDoc.get(name);
                if (cached != null)
                {
                    return cached;
                }
            }
        }
        return null;
    }

    protected static void putParagraphStyleInCache(
            ParagraphStyle style, Document selectorDocument)
    {
        String self = style.getSelf();
        if (paragraphStyleCacheBySelf == null)
        {
            paragraphStyleCacheBySelf = new Hashtable<>();
        }
        Dictionary<String, ParagraphStyle> internalCacheBySelf =
                paragraphStyleCacheBySelf.get(selectorDocument);
        if (internalCacheBySelf == null)
        {
            internalCacheBySelf = new Hashtable<>();
        }
        internalCacheBySelf.put(self, style);
        paragraphStyleCacheBySelf.put(selectorDocument, internalCacheBySelf);
        String name = style.getName();
        if (paragraphStyleCacheByName == null)
        {
            paragraphStyleCacheByName = new Hashtable<>();
        }
        Dictionary<String, ParagraphStyle> internalCacheByName =
                paragraphStyleCacheByName.get(selectorDocument);
        if (internalCacheByName == null)
        {
            internalCacheByName = new Hashtable<>();
        }
        internalCacheByName.put(name, style);
        paragraphStyleCacheByName.put(selectorDocument, internalCacheBySelf);
    }

    /**
     * cancella tutti gli stili di paragrafo in cache
     */
    public static void cleanupParagraphsStylesCache()
    {
        paragraphStyleCacheByName = null;
        paragraphStyleCacheBySelf = null;
        paragraphStylesChainCache = null;
    }

    /**
     * trova lo stile di paragrafo usando il nome dello stile
     *
     * @param supertemplateDocument il supertemplate su cui si cerca lo stile di
     * paragrafo
     * @param name il nome dello stile di paragrafo cercato
     * @return lo stile di paragrafo con il nome indicato o null se non viene
     * trovato
     * @throws IdmlLibException
     */
    public static ParagraphStyle retrieveParagraphStyleByName(
            Document supertemplateDocument, String name) throws IdmlLibException
    {
        ParagraphStyle cached = retrieveCachedParagraphStyleByName(
                supertemplateDocument, name);
        if (cached != null)
        {
            return cached;
        }
        return retrieveParagraphStyleBySelfOrName(supertemplateDocument, name,
                false);
    }

    /**
     * trova lo stile di paragrafo usando il nome o il self dello stile
     *
     * @param supertemplateDocument il supertemplate su cui si cerca lo stile di
     * paragrafo
     * @param nameOrSelf il nome o il self dello stile che si va cercando
     * @param useSelf se true interpreta il parametro come self, altrimenti lo
     * interpreta come nome
     * @return lo stile di paragrafo cercato o null se non trova nulla
     * @throws IdmlLibException
     */
    public static ParagraphStyle retrieveParagraphStyleBySelfOrName(
            Document supertemplateDocument, String nameOrSelf, boolean useSelf)
            throws IdmlLibException
    {
        Styles styles = supertemplateDocument.getStyles();
        RootParagraphStyleGroup rootParagraphStyleGroup =
                styles.getRootParagraphStyleGroup();
        return retrieveParagraphStyleBySelfOrNameInRootParagraphStyleGroupRec(
                rootParagraphStyleGroup, nameOrSelf, useSelf,
                supertemplateDocument);
    }

    /**
     * cerca lo stile di paragrafo ricorsivamente dentro il root paragraph style
     * group
     *
     * @param rootGroup il root paragraph style group
     * @param nameOrSelf il nome o il self dello stile che si va cercando
     * @param useSelf se true interpreta il parametro come self, altrimenti lo
     * interpreta come nome
     * @return lo stile di paragrafo cercato o null se non trova nulla
     */
    private static ParagraphStyle retrieveParagraphStyleBySelfOrNameInRootParagraphStyleGroupRec(
            RootParagraphStyleGroup rootGroup, String nameOrSelf,
            boolean useSelf, Document supertemplateDocument)
    {
        List<ParagraphStyle> paragraphStyleList =
                rootGroup.getParagraphStyleList();
        //la memorizzazione in cache rallenta molto, quindi memorizzo solo gli stili che vado a restituire
        for (ParagraphStyle style : paragraphStyleList)
        {
            putParagraphStyleInCache(style, supertemplateDocument);
            if (useSelf)
            {
                String self = style.getSelf();
                if (self.equals(nameOrSelf))
                {
                    putParagraphStyleInCache(style, supertemplateDocument);
                    return style;
                }
            } else
            {
                String name = style.getName();
                if (name.equals(nameOrSelf))
                {
                    putParagraphStyleInCache(style, supertemplateDocument);
                    return style;
                }
            }
        }
        List<ParagraphStyleGroup> paragraphStyleGroupList =
                rootGroup.getParagraphStyleGroupList();
        for (ParagraphStyleGroup subgroup : paragraphStyleGroupList)
        {
            ParagraphStyle found =
                    retrieveParagraphStyleBySelfOrNameInParagraphStyleGroupRec(
                            subgroup, nameOrSelf, useSelf, supertemplateDocument);
            if (found != null)
            {
                return found;
            }
        }
        return null;
    }

    /**
     * cerca lo stile di paragrafo ricorsivamente dentro un paragraph style
     * group
     *
     * @param group il paragraph style group in cui cerca il paragrafo
     * @param nameOrSelf il nome o il self dello stile che si va cercando
     * @param useSelf se true interpreta il parametro come self, altrimenti lo
     * interpreta come nome
     * @return lo stile di paragrafo cercato o null se non trova nulla
     */
    private static ParagraphStyle retrieveParagraphStyleBySelfOrNameInParagraphStyleGroupRec(
            ParagraphStyleGroup group, String nameOrSelf, boolean useSelf,
            Document supertemplateDocument)
    {
        List<ParagraphStyle> paragraphStyleList =
                group.getParagraphStyleList();
        for (ParagraphStyle style : paragraphStyleList)
        {
            putParagraphStyleInCache(style, supertemplateDocument);
            if (useSelf)
            {
                String self = style.getSelf();
                if (self.equals(nameOrSelf))
                {
                    putParagraphStyleInCache(style, supertemplateDocument);
                    return style;
                }
            } else
            {
                String name = style.getName();
                if (name.equals(nameOrSelf))
                {
                    putParagraphStyleInCache(style, supertemplateDocument);
                    return style;
                }
            }
        }
        List<ParagraphStyleGroup> paragraphStyleGroupList =
                group.getParagraphStyleGroupList();
        for (ParagraphStyleGroup subgroup : paragraphStyleGroupList)
        {
            ParagraphStyle found =
                    retrieveParagraphStyleBySelfOrNameInParagraphStyleGroupRec(
                            subgroup, nameOrSelf, useSelf, supertemplateDocument);
            if (found != null)
            {
                return found;
            }
        }
        return null;
    }

    /**
     * ricerca un elenco degli stili di paragrafo su cui è basato lo stile di
     * paragrafo corrente (dal più vicino al più lontano)
     *
     * @param supertemplateDocument il documento in cui si cerca la catena di
     * stili
     * @param currentStyle lo stile di paragrafo di cui si cerca la catena degli
     * stili su cui è basato
     * @return la lista degli stili su cui si basa
     * @throws IdmlLibException
     */
    public static List<ParagraphStyle> retrieveParagraphStylesCurrentAndBasedOnChain(
            Document supertemplateDocument, ParagraphStyle currentStyle) throws
            IdmlLibException
    {
        List<ParagraphStyle> cached =
                retrieveParagraphStylesChainFromCache(currentStyle,
                        supertemplateDocument);
        if (cached != null)
        {
            return cached;
        }
        return calculateParagraphStylesCurrentAndBasedOnChain(currentStyle,
                supertemplateDocument);
    }

    protected static List<ParagraphStyle> calculateParagraphStylesCurrentAndBasedOnChain(
            ParagraphStyle currentStyle,
            Document supertemplateDocument)
            throws IdmlLibException
    {
        List<ParagraphStyle> chain = new ArrayList<>();
        if (currentStyle == null)
        {
            return chain;
        }
        chain.add(currentStyle);//intanto aggiungo lo stile corrente
        ParagraphStyle examiningStyle = currentStyle;
        while (examiningStyle != null)
        {
            ParagraphStyle retrievedParentStyle = null;
            BasedOn basedOn = examiningStyle.getBasedOn();
            if (basedOn != null)
            {
                String basedOnReference = basedOn.getValueAsString();
                String selfPrefix = PARAGRAPH_STYLE_PREFIX;
                String basedOnReferenceWithPrefix = basedOnReference;
                if ((basedOnReference != null) && (!basedOnReference.isEmpty()) &&
                        (!basedOnReference.startsWith(selfPrefix)))
                {
                    //se il based on è ad esempio uno stile base, non mettono correttamente il self in idml percui devo aggiungere il prefisso
                    basedOnReferenceWithPrefix = selfPrefix + basedOnReference;
                }

                if ((basedOnReferenceWithPrefix != null))
                {
                    retrievedParentStyle = retrieveParagraphStyleBySelf(
                            supertemplateDocument, basedOnReferenceWithPrefix);
                    if (retrievedParentStyle == null)
                    {
                        //in casi estremi uso il valore passato, ma è improbabile
                        retrievedParentStyle = retrieveParagraphStyleBySelf(
                                supertemplateDocument,
                                basedOnReference);

                    }
                }

            }
            examiningStyle = retrievedParentStyle;
            if (examiningStyle != null)
            {
                chain.add(examiningStyle);
            }

        }
        putParagraphStylesChainInCache(chain, currentStyle,
                supertemplateDocument);
        return chain;
    }

    protected static Dictionary<Document, Dictionary<String, List<ParagraphStyle>>> paragraphStylesChainCache =
            null;

    protected static void putParagraphStylesChainInCache(
            List<ParagraphStyle> chain, ParagraphStyle key,
            Document supertemplateDocument)
    {
        if (paragraphStylesChainCache == null)
        {
            paragraphStylesChainCache = new Hashtable<>();
        }
        Dictionary<String, List<ParagraphStyle>> chainCacheOfDocument =
                paragraphStylesChainCache.get(supertemplateDocument);
        if (chainCacheOfDocument == null)
        {
            chainCacheOfDocument = new Hashtable<>();
            paragraphStylesChainCache.put(supertemplateDocument,
                    chainCacheOfDocument);
        }
        String self = key.getSelf();
        chainCacheOfDocument.put(self, chain);
    }

    protected static List<ParagraphStyle> retrieveParagraphStylesChainFromCache(
            ParagraphStyle key, Document supertemplateDocument)
    {
        if (key == null)
        {
            return null;
        }
        if (paragraphStylesChainCache != null)
        {
            Dictionary<String, List<ParagraphStyle>> chainCacheOfDocument =
                    paragraphStylesChainCache.get(supertemplateDocument);
            if (chainCacheOfDocument != null)
            {
                String self = key.getSelf();
                List<ParagraphStyle> get = chainCacheOfDocument.get(self);
                return get;
            }
        }
        return null;
    }

    protected static void putCellStyleInCache(
            CellStyle style, Document selectorDocument)
    {

        String self = style.getSelf();
        if (cellStyleCacheBySelf == null)
        {
            cellStyleCacheBySelf = new Hashtable<>();
        }
        Dictionary<String, CellStyle> internalCacheBySelf =
                cellStyleCacheBySelf.get(selectorDocument);
        if (internalCacheBySelf == null)
        {
            internalCacheBySelf = new Hashtable<>();
        }
        internalCacheBySelf.put(self, style);
        cellStyleCacheBySelf.put(selectorDocument, internalCacheBySelf);
        String name = style.getName();
        if (cellStyleCacheByName == null)
        {
            cellStyleCacheByName = new Hashtable<>();
        }
        Dictionary<String, CellStyle> internalCacheByName =
                cellStyleCacheByName.get(selectorDocument);
        if (internalCacheByName == null)
        {
            internalCacheByName = new Hashtable<>();
        }
        internalCacheByName.put(name, style);
        cellStyleCacheByName.put(selectorDocument, internalCacheBySelf);
    }

    /**
     * cancella tutti gli stili di cella in cache
     */
    public static void cleanupCellStylesCache()
    {
        cellStyleCacheByName = null;
        cellStyleCacheBySelf = null;
        cellStylesChainCache = null;
    }

    private static CellStyle retrieveCachedCellStyleBySelf(
            Document supertemplateDocument, String self)
    {
        if (cellStyleCacheBySelf != null)
        {
            Dictionary<String, CellStyle> cachedForDoc = cellStyleCacheBySelf.
                    get(supertemplateDocument);
            if (cachedForDoc != null)
            {
                CellStyle cached = cachedForDoc.get(self);
                if (cached != null)
                {
                    return cached;
                }
            }
        }
        return null;
    }

    private static CellStyle retrieveCachedCellStyleByName(
            Document supertemplateDocument, String name)
    {
        if (cellStyleCacheByName != null)
        {
            Dictionary<String, CellStyle> cachedForDoc = cellStyleCacheByName.
                    get(supertemplateDocument);
            if (cachedForDoc != null)
            {
                CellStyle cached = cachedForDoc.get(name);
                if (cached != null)
                {
                    return cached;
                }
            }
        }
        return null;
    }

    /**
     * trova lo stile di cella usando il self dello stile
     *
     * @param supertemplateDocument il supertemplate su cui si cerca lo stile di
     * cella
     * @param self il self dello stile di cella cercato
     * @return lo stile di cella con il self indicato o null se non viene
     * trovato
     * @throws IdmlLibException
     */
    public static CellStyle retrieveCellStyleBySelf(
            Document supertemplateDocument, String self) throws
            IdmlLibException
    {
        CellStyle cached = retrieveCachedCellStyleBySelf(supertemplateDocument,
                self);
        if (cached != null)
        {
            return cached;
        }
        return retrieveCellStyleBySelfOrName(supertemplateDocument, self,
                true);
    }

    /**
     * trova lo stile di cella usando il nome dello stile
     *
     * @param supertemplateDocument il supertemplate su cui si cerca lo stile di
     * cella
     * @param name il nome dello stile di cella cercato
     * @return lo stile di cella con il nome indicato o null se non viene
     * trovato
     * @throws IdmlLibException
     */
    public static CellStyle retrieveCellStyleByName(
            Document supertemplateDocument, String name) throws IdmlLibException
    {
        CellStyle cached = retrieveCachedCellStyleByName(supertemplateDocument,
                name);
        if (cached != null)
        {
            return cached;
        }
        return retrieveCellStyleBySelfOrName(supertemplateDocument, name,
                false);
    }

    /**
     * trova lo stile di cella usando il nome o il self dello stile
     *
     * @param supertemplateDocument il supertemplate su cui si cerca lo stile di
     * cella
     * @param nameOrSelf il nome o il self dello stile che si va cercando
     * @param useSelf se true interpreta il parametro come self, altrimenti lo
     * interpreta come nome
     * @return lo stile di cella cercato o null se non trova nulla
     * @throws IdmlLibException
     */
    public static CellStyle retrieveCellStyleBySelfOrName(
            Document supertemplateDocument, String nameOrSelf, boolean useSelf)
            throws IdmlLibException
    {
        Styles styles = supertemplateDocument.getStyles();
        RootCellStyleGroup rootCellStyleGroup =
                styles.getRootCellStyleGroup();
        return retrieveCellStyleBySelfOrNameInRootCellStyleGroupRec(
                rootCellStyleGroup, nameOrSelf, useSelf, supertemplateDocument);
    }

    /**
     * cerca lo stile di cella ricorsivamente dentro il root cell style group
     *
     * @param rootGroup il root cell style group
     * @param nameOrSelf il nome o il self dello stile che si va cercando
     * @param useSelf se true interpreta il parametro come self, altrimenti lo
     * interpreta come nome
     * @return lo stile di cella cercato o null se non trova nulla
     */
    private static CellStyle retrieveCellStyleBySelfOrNameInRootCellStyleGroupRec(
            RootCellStyleGroup rootGroup, String nameOrSelf, boolean useSelf,
            Document supertemplateDocument)
    {
        List<CellStyle> cellStyleList =
                rootGroup.getCellStyleList();
        for (CellStyle style : cellStyleList)
        {
            putCellStyleInCache(style, supertemplateDocument);
            if (useSelf)
            {
                String self = style.getSelf();
                if (self.equals(nameOrSelf))
                {
                    putCellStyleInCache(style, supertemplateDocument);
                    return style;
                }
            } else
            {
                String name = style.getName();
                if (name.equals(nameOrSelf))
                {
                    putCellStyleInCache(style, supertemplateDocument);
                    return style;
                }
            }
        }
        List<CellStyleGroup> cellStyleGroupList =
                rootGroup.getCellStyleGroupList();
        for (CellStyleGroup subgroup : cellStyleGroupList)
        {
            CellStyle found =
                    retrieveCellStyleBySelfOrNameInCellStyleGroupRec(
                            subgroup, nameOrSelf, useSelf, supertemplateDocument);
            if (found != null)
            {
                return found;
            }
        }
        return null;
    }

    /**
     * cerca lo stile di cella ricorsivamente dentro un cell style group
     *
     * @param group il cell style group in cui cerca il paragrafo
     * @param nameOrSelf il nome o il self dello stile che si va cercando
     * @param useSelf se true interpreta il parametro come self, altrimenti lo
     * interpreta come nome
     * @return lo stile di cella cercato o null se non trova nulla
     */
    private static CellStyle retrieveCellStyleBySelfOrNameInCellStyleGroupRec(
            CellStyleGroup group, String nameOrSelf, boolean useSelf,
            Document supertemplateDocument)
    {
        List<CellStyle> cellStyleList =
                group.getCellStyleList();
        for (CellStyle style : cellStyleList)
        {
            putCellStyleInCache(style, supertemplateDocument);
            if (useSelf)
            {
                String self = style.getSelf();
                if (self.equals(nameOrSelf))
                {
                    putCellStyleInCache(style, supertemplateDocument);
                    return style;
                }
            } else
            {
                String name = style.getName();
                if (name.equals(nameOrSelf))
                {
                    putCellStyleInCache(style, supertemplateDocument);
                    return style;
                }
            }
        }
        List<CellStyleGroup> cellStyleGroupList =
                group.getCellStyleGroupList();
        for (CellStyleGroup subgroup : cellStyleGroupList)
        {
            CellStyle found =
                    retrieveCellStyleBySelfOrNameInCellStyleGroupRec(
                            subgroup, nameOrSelf, useSelf, supertemplateDocument);
            if (found != null)
            {
                return found;
            }
        }
        return null;
    }

    /**
     * ricerca un elenco degli stili di cella su cui è basato lo stile di cella
     * corrente (dal più vicino al più lontano)
     *
     * @param supertemplateDocument il documento in cui si cerca la catena di
     * stili
     * @param currentStyle lo stile di cella di cui si cerca la catena degli
     * stili su cui è basato
     * @return la lista degli stili su cui si basa
     * @throws IdmlLibException
     */
    public static List<CellStyle> retrieveCellStylesCurrentAndBasedOnChain(
            Document supertemplateDocument, CellStyle currentStyle) throws
            IdmlLibException
    {
        List<CellStyle> cached =
                retrieveCellStylesChainFromCache(currentStyle,
                        supertemplateDocument);
        if (cached != null)
        {
            return cached;
        }
        return calculateCellStylesCurrentAndBasedOnChain(currentStyle,
                supertemplateDocument);
    }

    protected static List<CellStyle> calculateCellStylesCurrentAndBasedOnChain(
            CellStyle currentStyle,
            Document supertemplateDocument)
            throws IdmlLibException
    {
        List<CellStyle> chain = new ArrayList<>();
        if (currentStyle == null)
        {
            return chain;
        }
        chain.add(currentStyle);//intanto aggiungo lo stile corrente
        CellStyle examiningStyle = currentStyle;
        while (examiningStyle != null)
        {
            CellStyle retrievedParentStyle = null;
            BasedOn basedOn = examiningStyle.getBasedOn();
            if (basedOn != null)
            {
                String basedOnReference = basedOn.getValueAsString();
                String selfPrefix = CELL_STYLE_PREFIX;
                String basedOnReferenceWithPrefix = basedOnReference;
                if ((basedOnReference != null) && (!basedOnReference.isEmpty()) &&
                        (!basedOnReference.startsWith(selfPrefix)))
                {
                    //se il based on è ad esempio uno stile base, non mettono correttamente il self in idml percui devo aggiungere il prefisso
                    basedOnReferenceWithPrefix = selfPrefix + basedOnReference;
                }

                if ((basedOnReferenceWithPrefix != null))
                {

                    retrievedParentStyle = retrieveCellStyleBySelf(
                            supertemplateDocument,
                            basedOnReferenceWithPrefix);
                    if (retrievedParentStyle == null)
                    {
                        //caso estremo nel caso non vi sia il prefisso ma estremamente improbabile
                        retrievedParentStyle = retrieveCellStyleBySelf(
                                supertemplateDocument,
                                basedOnReference);

                    }
                }

            }
            examiningStyle = retrievedParentStyle;
            if (examiningStyle != null)
            {
                chain.add(examiningStyle);
            }

        }
        putCellStylesChainInCache(chain, currentStyle, supertemplateDocument);
        return chain;
    }

    protected static Dictionary<Document, Dictionary<String, List<CellStyle>>> cellStylesChainCache =
            null;

    protected static void putCellStylesChainInCache(List<CellStyle> chain,
                                                    CellStyle key,
                                                    Document supertemplateDocument)
    {
        if (cellStylesChainCache == null)
        {
            cellStylesChainCache = new Hashtable<>();
        }
        Dictionary<String, List<CellStyle>> chainCacheOfDocument =
                cellStylesChainCache.get(supertemplateDocument);
        if (chainCacheOfDocument == null)
        {
            chainCacheOfDocument = new Hashtable<>();
            cellStylesChainCache.put(supertemplateDocument,
                    chainCacheOfDocument);
        }
        String self = key.getSelf();
        chainCacheOfDocument.put(self, chain);
    }

    protected static List<CellStyle> retrieveCellStylesChainFromCache(
            CellStyle key, Document supertemplateDocument)
    {
        if (key == null)
        {
            return null;
        }
        if (cellStylesChainCache != null)
        {
            Dictionary<String, List<CellStyle>> chainCacheOfDocument =
                    cellStylesChainCache.get(supertemplateDocument);
            if (chainCacheOfDocument != null)
            {
                String self = key.getSelf();
                List<CellStyle> get = chainCacheOfDocument.get(self);
                return get;
            }
        }
        return null;
    }

    protected static void putTableStyleInCache(
            TableStyle style, Document selectorDocument)
    {
        String self = style.getSelf();
        if (tableStyleCacheBySelf == null)
        {
            tableStyleCacheBySelf = new Hashtable<>();
        }
        Dictionary<String, TableStyle> internalCacheBySelf =
                tableStyleCacheBySelf.get(selectorDocument);
        if (internalCacheBySelf == null)
        {
            internalCacheBySelf = new Hashtable<>();
        }
        internalCacheBySelf.put(self, style);
        tableStyleCacheBySelf.put(selectorDocument, internalCacheBySelf);
        String name = style.getName();
        if (tableStyleCacheByName == null)
        {
            tableStyleCacheByName = new Hashtable<>();
        }
        Dictionary<String, TableStyle> internalCacheByName =
                tableStyleCacheByName.get(selectorDocument);
        if (internalCacheByName == null)
        {
            internalCacheByName = new Hashtable<>();
        }
        internalCacheByName.put(name, style);
        tableStyleCacheByName.put(selectorDocument, internalCacheBySelf);
    }

    /**
     * cancella tutti gli stili di tabella in cache
     */
    public static void cleanupTableStylesCache()
    {
        tableStyleCacheByName = null;
        tableStyleCacheBySelf = null;
        tableStylesChainCache = null;
    }

    private static TableStyle retrieveCachedTableStyleBySelf(
            Document supertemplateDocument, String self)
    {
        if (tableStyleCacheBySelf != null)
        {
            Dictionary<String, TableStyle> cachedForDoc = tableStyleCacheBySelf.
                    get(supertemplateDocument);
            if (cachedForDoc != null)
            {
                TableStyle cached = cachedForDoc.get(self);
                if (cached != null)
                {
                    return cached;
                }
            }
        }
        return null;
    }

    private static TableStyle retrieveCachedTableStyleByName(
            Document supertemplateDocument, String name)
    {
        if (tableStyleCacheByName != null)
        {
            Dictionary<String, TableStyle> cachedForDoc = tableStyleCacheByName.
                    get(supertemplateDocument);
            if (cachedForDoc != null)
            {
                TableStyle cached = cachedForDoc.get(name);
                if (cached != null)
                {
                    return cached;
                }
            }
        }
        return null;
    }

    /**
     * trova lo stile di tabella usando il self dello stile
     *
     * @param supertemplateDocument il supertemplate su cui si cerca lo stile di
     * tabella
     * @param self il self dello stile di tabella cercato
     * @return lo stile di tabella con il self indicato o null se non viene
     * trovato
     * @throws IdmlLibException
     */
    public static TableStyle retrieveTableStyleBySelf(
            Document supertemplateDocument, String self) throws
            IdmlLibException
    {
        TableStyle cached =
                retrieveCachedTableStyleBySelf(supertemplateDocument, self);
        if (cached != null)
        {
            return cached;
        }
        return retrieveTableStyleBySelfOrName(supertemplateDocument, self,
                true);
    }

    /**
     * trova lo stile di tabella usando il nome dello stile
     *
     * @param supertemplateDocument il supertemplate su cui si cerca lo stile di
     * tabella
     * @param name il nome dello stile di tabella cercato
     * @return lo stile di tabella con il nome indicato o null se non viene
     * trovato
     * @throws IdmlLibException
     */
    public static TableStyle retrieveTableStyleByName(
            Document supertemplateDocument, String name) throws IdmlLibException
    {
        TableStyle cached =
                retrieveCachedTableStyleByName(supertemplateDocument, name);
        if (cached != null)
        {
            return cached;
        }
        return retrieveTableStyleBySelfOrName(supertemplateDocument, name,
                false);
    }

    /**
     * trova lo stile di tabella usando il nome o il self dello stile
     *
     * @param supertemplateDocument il supertemplate su cui si cerca lo stile di
     * tabella
     * @param nameOrSelf il nome o il self dello stile che si va cercando
     * @param useSelf se true interpreta il parametro come self, altrimenti lo
     * interpreta come nome
     * @return lo stile di tabella cercato o null se non trova nulla
     * @throws IdmlLibException
     */
    public static TableStyle retrieveTableStyleBySelfOrName(
            Document supertemplateDocument, String nameOrSelf, boolean useSelf)
            throws IdmlLibException
    {
        Styles styles = supertemplateDocument.getStyles();
        RootTableStyleGroup rootGroup =
                styles.getRootTableStyleGroup();
        return retrieveTableStyleBySelfOrNameInRootTableStyleGroupRec(rootGroup,
                nameOrSelf, useSelf, supertemplateDocument);
    }

    /**
     * cerca lo stile di tabella ricorsivamente dentro il root table style group
     *
     * @param rootGroup il root table style group
     * @param nameOrSelf il nome o il self dello stile che si va cercando
     * @param useSelf se true interpreta il parametro come self, altrimenti lo
     * interpreta come nome
     * @return lo stile di tabella cercato o null se non trova nulla
     */
    private static TableStyle retrieveTableStyleBySelfOrNameInRootTableStyleGroupRec(
            RootTableStyleGroup rootGroup, String nameOrSelf, boolean useSelf,
            Document supertemplateDocument)
    {
        List<TableStyle> currentLevelStylesList =
                rootGroup.getTableStyleList();
        for (TableStyle style : currentLevelStylesList)
        {
            putTableStyleInCache(style, supertemplateDocument);
            if (useSelf)
            {
                String self = style.getSelf();
                if (self.equals(nameOrSelf))
                {
                    putTableStyleInCache(style, supertemplateDocument);
                    return style;
                }
            } else
            {
                String name = style.getName();
                if (name.equals(nameOrSelf))
                {
                    putTableStyleInCache(style, supertemplateDocument);
                    return style;
                }
            }
        }
        List<TableStyleGroup> subGroupsList =
                rootGroup.getTableStyleGroupList();
        for (TableStyleGroup subgroup : subGroupsList)
        {
            TableStyle found =
                    retrieveTableStyleBySelfOrNameInTableStyleGroupRec(
                            subgroup, nameOrSelf, useSelf, supertemplateDocument);
            if (found != null)
            {
                return found;
            }
        }
        return null;
    }

    /**
     * cerca lo stile di tabella ricorsivamente dentro un table style group
     *
     * @param group il table style group in cui cerca il paragrafo
     * @param nameOrSelf il nome o il self dello stile che si va cercando
     * @param useSelf se true interpreta il parametro come self, altrimenti lo
     * interpreta come nome
     * @return lo stile di tabella cercato o null se non trova nulla
     */
    private static TableStyle retrieveTableStyleBySelfOrNameInTableStyleGroupRec(
            TableStyleGroup group, String nameOrSelf, boolean useSelf,
            Document supertemplateDocument)
    {
        List<TableStyle> currentLevelStylesList =
                group.getTableStyleList();
        for (TableStyle style : currentLevelStylesList)
        {
            putTableStyleInCache(style, supertemplateDocument);
            if (useSelf)
            {
                String self = style.getSelf();
                if (self.equals(nameOrSelf))
                {
                    putTableStyleInCache(style, supertemplateDocument);
                    return style;
                }
            } else
            {
                String name = style.getName();
                if (name.equals(nameOrSelf))
                {
                    putTableStyleInCache(style, supertemplateDocument);
                    return style;
                }
            }
        }
        List<TableStyleGroup> subGroupsList =
                group.getTableStyleGroupList();
        for (TableStyleGroup subgroup : subGroupsList)
        {
            TableStyle found =
                    retrieveTableStyleBySelfOrNameInTableStyleGroupRec(
                            subgroup, nameOrSelf, useSelf, supertemplateDocument);
            if (found != null)
            {
                return found;
            }
        }
        return null;
    }

    /**
     * ricerca un elenco degli stili di tabella su cui è basato lo stile di
     * tabella corrente (dal più vicino al più lontano)
     *
     * @param supertemplateDocument il documento in cui si cerca la catena di
     * stili
     * @param currentStyle lo stile di tabella di cui si cerca la catena degli
     * stili su cui è basato
     * @return la lista degli stili su cui si basa
     * @throws IdmlLibException
     */
    public static List<TableStyle> retrieveTableStylesCurrentAndBasedOnChain(
            Document supertemplateDocument, TableStyle currentStyle) throws
            IdmlLibException
    {
        List<TableStyle> cached =
                retrieveTableStylesChainFromCache(currentStyle,
                        supertemplateDocument);
        if (cached != null)
        {
            return cached;
        }
        return calculateTableStylesCurrentAndBasedOnChain(currentStyle,
                supertemplateDocument);
    }

    protected static List<TableStyle> calculateTableStylesCurrentAndBasedOnChain(
            TableStyle currentStyle,
            Document supertemplateDocument)
            throws IdmlLibException
    {
        List<TableStyle> chain = new ArrayList<>();
        if (currentStyle == null)
        {
            return chain;
        }
        chain.add(currentStyle);//intanto aggiungo lo stile corrente
        TableStyle examiningStyle = currentStyle;
        while (examiningStyle != null)
        {
            TableStyle retrievedParentStyle = null;
            BasedOn basedOn = examiningStyle.getBasedOn();
            if (basedOn != null)
            {
                String basedOnReference = basedOn.getValueAsString();
                String selfPrefix = TABLE_STYLE_PREFIX;
                String basedOnReferenceWithPrefix = basedOnReference;
                if ((basedOnReference != null) && (!basedOnReference.isEmpty()) &&
                        (!basedOnReference.startsWith(selfPrefix)))
                {
                    //se il based on è ad esempio uno stile base, non mettono correttamente il self in idml percui devo aggiungere il prefisso
                    basedOnReferenceWithPrefix = selfPrefix + basedOnReference;
                }

                if ((basedOnReferenceWithPrefix != null))
                {

                    retrievedParentStyle = retrieveTableStyleBySelf(
                            supertemplateDocument,
                            basedOnReferenceWithPrefix);
                    if (retrievedParentStyle == null)
                    {
                        //caso improbabile in cui non ci sia il prefisso
                        retrievedParentStyle = retrieveTableStyleBySelf(
                                supertemplateDocument,
                                basedOnReference);

                    }
                }

            }
            examiningStyle = retrievedParentStyle;
            if (examiningStyle != null)
            {
                chain.add(examiningStyle);
            }

        }
        putTableStylesChainInCache(chain, currentStyle, supertemplateDocument);
        return chain;
    }

    protected static Dictionary<Document, Dictionary<String, List<TableStyle>>> tableStylesChainCache =
            null;

    protected static void putTableStylesChainInCache(List<TableStyle> chain,
                                                     TableStyle key,
                                                     Document supertemplateDocument)
    {
        if (tableStylesChainCache == null)
        {
            tableStylesChainCache = new Hashtable<>();
        }
        Dictionary<String, List<TableStyle>> chainCacheOfDocument =
                tableStylesChainCache.get(supertemplateDocument);
        if (chainCacheOfDocument == null)
        {
            chainCacheOfDocument = new Hashtable<>();
            tableStylesChainCache.put(supertemplateDocument,
                    chainCacheOfDocument);
        }
        String self = key.getSelf();
        chainCacheOfDocument.put(self, chain);
    }

    protected static List<TableStyle> retrieveTableStylesChainFromCache(
            TableStyle key, Document supertemplateDocument)
    {
        if (key == null)
        {
            return null;
        }
        if (tableStylesChainCache != null)
        {
            Dictionary<String, List<TableStyle>> chainCacheOfDocument =
                    tableStylesChainCache.get(supertemplateDocument);
            if (chainCacheOfDocument != null)
            {
                String self = key.getSelf();
                List<TableStyle> get = chainCacheOfDocument.get(self);
                return get;
            }
        }
        return null;
    }

    protected static void putObjectStyleInCache(
            ObjectStyle style, Document selectorDocument)
    {

        String self = style.getSelf();
        if (objectStyleCacheBySelf == null)
        {
            objectStyleCacheBySelf = new Hashtable<>();
        }
        Dictionary<String, ObjectStyle> internalCacheBySelf =
                objectStyleCacheBySelf.get(selectorDocument);
        if (internalCacheBySelf == null)
        {
            internalCacheBySelf = new Hashtable<>();
        }
        internalCacheBySelf.put(self, style);
        objectStyleCacheBySelf.put(selectorDocument, internalCacheBySelf);
        String name = style.getName();
        if (objectStyleCacheByName == null)
        {
            objectStyleCacheByName = new Hashtable<>();
        }
        Dictionary<String, ObjectStyle> internalCacheByName =
                objectStyleCacheByName.get(selectorDocument);
        if (internalCacheByName == null)
        {
            internalCacheByName = new Hashtable<>();
        }
        internalCacheByName.put(name, style);
        objectStyleCacheByName.put(selectorDocument, internalCacheBySelf);
    }

    /**
     * cancella tutti gli stili di oggetto in cache
     */
    public static void cleanupObjectStylesCache()
    {
        objectStyleCacheByName = null;
        objectStyleCacheBySelf = null;
        objectStylesChainCache = null;
    }

    private static ObjectStyle retrieveCachedObjectStyleBySelf(
            Document supertemplateDocument, String self)
    {
        if (objectStyleCacheBySelf != null)
        {
            Dictionary<String, ObjectStyle> cachedForDoc =
                    objectStyleCacheBySelf.get(supertemplateDocument);
            if (cachedForDoc != null)
            {
                ObjectStyle cached = cachedForDoc.get(self);
                if (cached != null)
                {
                    return cached;
                }
            }
        }
        return null;
    }

    private static ObjectStyle retrieveCachedObjectStyleByName(
            Document supertemplateDocument, String name)
    {
        if (objectStyleCacheByName != null)
        {
            Dictionary<String, ObjectStyle> cachedForDoc =
                    objectStyleCacheByName.get(supertemplateDocument);
            if (cachedForDoc != null)
            {
                ObjectStyle cached = cachedForDoc.get(name);
                if (cached != null)
                {
                    return cached;
                }
            }
        }
        return null;
    }

    /**
     * trova lo stile di oggetto usando il self dello stile
     *
     * @param supertemplateDocument il supertemplate su cui si cerca lo stile di
     * oggetto
     * @param self il self dello stile di oggetto cercato
     * @return lo stile di oggetto con il self indicato o null se non viene
     * trovato
     * @throws IdmlLibException
     */
    public static ObjectStyle retrieveObjectStyleBySelf(
            Document supertemplateDocument, String self) throws
            IdmlLibException
    {
        ObjectStyle cached = retrieveCachedObjectStyleBySelf(
                supertemplateDocument, self);
        if (cached != null)
        {
            return cached;
        }
        return retrieveObjectStyleBySelfOrName(supertemplateDocument, self,
                true);
    }

    /**
     * trova lo stile di oggetto usando il nome dello stile
     *
     * @param supertemplateDocument il supertemplate su cui si cerca lo stile di
     * oggetto
     * @param name il nome dello stile di oggetto cercato
     * @return lo stile di oggetto con il nome indicato o null se non viene
     * trovato
     * @throws IdmlLibException
     */
    public static ObjectStyle retrieveObjectStyleByName(
            Document supertemplateDocument,
            String name) throws
            IdmlLibException
    {
        ObjectStyle cached = retrieveCachedObjectStyleByName(
                supertemplateDocument, name);
        if (cached != null)
        {
            return cached;
        }
        return retrieveObjectStyleBySelfOrName(supertemplateDocument, name,
                false);
    }

    /**
     * trova lo stile di oggetto usando il nome o il self dello stile
     *
     * @param supertemplateDocument il supertemplate su cui si cerca lo stile di
     * oggetto
     * @param nameOrSelf il nome o il self dello stile che si va cercando
     * @param useSelf se true interpreta il parametro come self, altrimenti lo
     * interpreta come nome
     * @return lo stile di oggetto cercato o null se non trova nulla
     * @throws IdmlLibException
     */
    public static ObjectStyle retrieveObjectStyleBySelfOrName(
            Document supertemplateDocument,
            String nameOrSelf,
            boolean useSelf)
            throws IdmlLibException
    {
        Styles styles = supertemplateDocument.getStyles();
        RootObjectStyleGroup rootGroup =
                styles.getRootObjectStyleGroup();
        return retrieveObjectStyleBySelfOrNameInRootObjectStyleGroupRec(
                rootGroup,
                nameOrSelf, useSelf, supertemplateDocument);
    }

    /**
     * cerca lo stile di oggetto ricorsivamente dentro il root object style
     * group
     *
     * @param rootGroup il root object style group
     * @param nameOrSelf il nome o il self dello stile che si va cercando
     * @param useSelf se true interpreta il parametro come self, altrimenti lo
     * interpreta come nome
     * @return lo stile di oggetto cercato o null se non trova nulla
     */
    private static ObjectStyle retrieveObjectStyleBySelfOrNameInRootObjectStyleGroupRec(
            RootObjectStyleGroup rootGroup,
            String nameOrSelf,
            boolean useSelf,
            Document supertemplateDocument)
    {
        List<ObjectStyle> currentLevelStylesList =
                rootGroup.getObjectStyleList();
        for (ObjectStyle style : currentLevelStylesList)
        {
            putObjectStyleInCache(style, supertemplateDocument);
            if (useSelf)
            {
                String self = style.getSelf();
                if (self.equals(nameOrSelf))
                {
                    putObjectStyleInCache(style, supertemplateDocument);
                    return style;
                }
            } else
            {
                String name = style.getName();
                if (name.equals(nameOrSelf))
                {
                    putObjectStyleInCache(style, supertemplateDocument);
                    return style;
                }
            }
        }
        List<ObjectStyleGroup> subGroupsList =
                rootGroup.getObjectStyleGroupList();
        for (ObjectStyleGroup subgroup : subGroupsList)
        {
            ObjectStyle found =
                    retrieveObjectStyleBySelfOrNameInObjectStyleGroupRec(
                            subgroup, nameOrSelf, useSelf, supertemplateDocument);
            if (found != null)
            {
                return found;
            }
        }
        return null;
    }

    /**
     * cerca lo stile di oggetto ricorsivamente dentro un object style group
     *
     * @param group il object style group in cui cerca il paragrafo
     * @param nameOrSelf il nome o il self dello stile che si va cercando
     * @param useSelf se true interpreta il parametro come self, altrimenti lo
     * interpreta come nome
     * @return lo stile di oggetto cercato o null se non trova nulla
     */
    private static ObjectStyle retrieveObjectStyleBySelfOrNameInObjectStyleGroupRec(
            ObjectStyleGroup group,
            String nameOrSelf,
            boolean useSelf,
            Document supertemplateDocument)
    {
        List<ObjectStyle> currentLevelStylesList =
                group.getObjectStyleList();
        for (ObjectStyle style : currentLevelStylesList)
        {
            putObjectStyleInCache(style, supertemplateDocument);
            if (useSelf)
            {
                String self = style.getSelf();
                if (self.equals(nameOrSelf))
                {
                    putObjectStyleInCache(style, supertemplateDocument);
                    return style;
                }
            } else
            {
                String name = style.getName();
                if (name.equals(nameOrSelf))
                {
                    putObjectStyleInCache(style, supertemplateDocument);
                    return style;
                }
            }
        }
        List<ObjectStyleGroup> subGroupsList =
                group.getObjectStyleGroupList();
        for (ObjectStyleGroup subgroup : subGroupsList)
        {
            ObjectStyle found =
                    retrieveObjectStyleBySelfOrNameInObjectStyleGroupRec(
                            subgroup, nameOrSelf, useSelf, supertemplateDocument);
            if (found != null)
            {
                return found;
            }
        }
        return null;
    }

    /**
     * ricerca un elenco degli stili di oggetto su cui è basato lo stile di
     * oggetto corrente (dal più vicino al più lontano)
     *
     * @param supertemplateDocument il documento in cui si cerca la catena di
     * stili
     * @param currentStyle lo stile di oggetto di cui si cerca la catena degli
     * stili su cui è basato
     * @return la lista degli stili su cui si basa
     * @throws IdmlLibException
     */
    public static List<ObjectStyle> retrieveObjectStylesCurrentAndBasedOnChain(
            Document supertemplateDocument,
            ObjectStyle currentStyle)
            throws IdmlLibException
    {
        List<ObjectStyle> cached =
                retrieveObjectStylesChainFromCache(currentStyle,
                        supertemplateDocument);
        if (cached != null)
        {
            return cached;
        }
        return calculateObjectStylesCurrentAndBasedOnChain(currentStyle,
                supertemplateDocument);
    }

    protected static List<ObjectStyle> calculateObjectStylesCurrentAndBasedOnChain(
            ObjectStyle currentStyle,
            Document supertemplateDocument)
            throws IdmlLibException
    {
        List<ObjectStyle> chain = new ArrayList<>();
        if (currentStyle == null)
        {
            return chain;
        }
        chain.add(currentStyle);//intanto aggiungo lo stile corrente
        ObjectStyle examiningStyle = currentStyle;
        while (examiningStyle != null)
        {
            ObjectStyle retrievedParentStyle = null;
            BasedOn basedOn = examiningStyle.getBasedOn();
            if (basedOn != null)
            {
                String basedOnReference = basedOn.getValueAsString();
                String selfPrefix = OBJECT_STYLE_PREFIX;
                String basedOnReferenceWithPrefix = basedOnReference;
                if ((basedOnReference != null) && (!basedOnReference.isEmpty()) &&
                        (!basedOnReference.startsWith(selfPrefix)))
                {
                    //se il based on è ad esempio uno stile base, non mettono correttamente il self in idml percui devo aggiungere il prefisso
                    basedOnReferenceWithPrefix = selfPrefix + basedOnReference;
                }

                if ((basedOnReferenceWithPrefix != null))
                {

                    retrievedParentStyle = retrieveObjectStyleBySelf(
                            supertemplateDocument,
                            basedOnReference);
                    if (retrievedParentStyle == null)
                    {
                        //caso improbabile in cui non ci sia il prefisso
                        retrievedParentStyle = retrieveObjectStyleBySelf(
                                supertemplateDocument,
                                basedOnReference);

                    }
                }

            }
            examiningStyle = retrievedParentStyle;
            if (examiningStyle != null)
            {
                chain.add(examiningStyle);
            }

        }
        putObjectStylesChainInCache(chain, currentStyle, supertemplateDocument);
        return chain;
    }

    protected static Dictionary<Document, Dictionary<String, List<ObjectStyle>>> objectStylesChainCache =
            null;

    protected static void putObjectStylesChainInCache(List<ObjectStyle> chain,
                                                      ObjectStyle key,
                                                      Document supertemplateDocument)
    {
        if (objectStylesChainCache == null)
        {
            objectStylesChainCache = new Hashtable<>();
        }
        Dictionary<String, List<ObjectStyle>> chainCacheOfDocument =
                objectStylesChainCache.get(supertemplateDocument);
        if (chainCacheOfDocument == null)
        {
            chainCacheOfDocument = new Hashtable<>();
            objectStylesChainCache.put(supertemplateDocument,
                    chainCacheOfDocument);
        }
        String self = key.getSelf();
        chainCacheOfDocument.put(self, chain);
    }

    protected static List<ObjectStyle> retrieveObjectStylesChainFromCache(
            ObjectStyle key, Document supertemplateDocument)
    {
        if (key == null)
        {
            return null;
        }
        if (objectStylesChainCache != null)
        {
            Dictionary<String, List<ObjectStyle>> chainCacheOfDocument =
                    objectStylesChainCache.get(supertemplateDocument);
            if (chainCacheOfDocument != null)
            {
                String self = key.getSelf();
                List<ObjectStyle> get = chainCacheOfDocument.get(self);
                return get;
            }
        }
        return null;
    }

    public static Color retrieveColorBySelf(
            Document supertemplateDocument, String self) throws IdmlLibException
    {

        Color cached = retrieveCachedColorBySelf(supertemplateDocument, self);
        if (cached != null)
        {
            return cached;
        }
        return retrieveColorBySelfInner(supertemplateDocument, self);
    }

    public static Color retrieveColorBySelfInner(Document supertemplateDocument,
                                                 String nameOrSelf) throws
            IdmlLibException
    {
        Graphic graphic = supertemplateDocument.getGraphic();
        List<Color> colorList = graphic.getColorList();
        for (Color color : colorList)
        {
            putColorInCache(color, supertemplateDocument);
            if (color.getSelf().equals(nameOrSelf))
            {
                putColorInCache(color, supertemplateDocument);
                return color;
            }
        }
        return null;
    }

    private static Color retrieveCachedColorBySelf(
            Document supertemplateDocument, String self)
    {
        if (colorsCacheBySelf != null)
        {
            Dictionary<String, Color> cachedForDoc =
                    colorsCacheBySelf.get(supertemplateDocument);
            if (cachedForDoc != null)
            {
                Color cached = cachedForDoc.get(self);
                if (cached != null)
                {
                    return cached;
                }
            }
        }
        return null;
    }

    protected static void putColorInCache(
            Color color, Document selectorDocument)
    {

        if (color != null)
        {
            String self = color.getSelf();
            if (colorsCacheBySelf == null)
            {
                colorsCacheBySelf = new Hashtable<>();
            }
            Dictionary<String, Color> internalCacheBySelf =
                    colorsCacheBySelf.get(selectorDocument);
            if (internalCacheBySelf == null)
            {
                internalCacheBySelf = new Hashtable<>();
            }
            internalCacheBySelf.put(self, color);
            colorsCacheBySelf.put(selectorDocument, internalCacheBySelf);
        }

    }

    public static void cleanupColorsCache()
    {
        colorsCacheBySelf = null;
    }

    public static Tint retrieveTintBySelf(
            Document supertemplateDocument, String self) throws IdmlLibException
    {

        Tint cached = retrieveCachedTintBySelf(supertemplateDocument, self);
        if (cached != null)
        {
            return cached;
        }
        return retrieveTintBySelfInner(supertemplateDocument, self);
    }

    public static Tint retrieveTintBySelfInner(Document supertemplateDocument,
                                               String nameOrSelf) throws
            IdmlLibException
    {
        Graphic graphic = supertemplateDocument.getGraphic();
        List<Tint> tintList = graphic.getTintList();
        for (Tint tint : tintList)
        {
            putTintInCache(tint, supertemplateDocument);
            if (tint.getSelf().equals(nameOrSelf))
            {
                putTintInCache(tint, supertemplateDocument);
                return tint;
            }
        }
        return null;
    }

    private static Tint retrieveCachedTintBySelf(
            Document supertemplateDocument, String self)
    {
        if (tintsCacheBySelf != null)
        {
            Dictionary<String, Tint> cachedForDoc =
                    tintsCacheBySelf.get(supertemplateDocument);
            if (cachedForDoc != null)
            {
                Tint cached = cachedForDoc.get(self);
                if (cached != null)
                {
                    return cached;
                }
            }
        }
        return null;
    }

    protected static void putTintInCache(
            Tint tint, Document selectorDocument)
    {

        if (tint != null)
        {
            String self = tint.getSelf();
            if (tintsCacheBySelf == null)
            {
                tintsCacheBySelf = new Hashtable<>();
            }
            Dictionary<String, Tint> internalCacheBySelf =
                    tintsCacheBySelf.get(selectorDocument);
            if (internalCacheBySelf == null)
            {
                internalCacheBySelf = new Hashtable<>();
            }
            internalCacheBySelf.put(self, tint);
            tintsCacheBySelf.put(selectorDocument, internalCacheBySelf);
        }

    }

    public static void cleanupTintsCache()
    {
        tintsCacheBySelf = null;

    }

    public static TintAndColorWrapper createTintAndColorWrapperBySelf(
            Document supertemplateDocument, String nameOrSelf) throws
            IdmlLibException
    {
        if (nameOrSelf == null)
        {
            return null;
        }
        if (nameOrSelf.equals(SWATCH_NONE))//se non ha colore, devo restituire null
        {
            return null;
        }
        if (nameOrSelf.startsWith(TINT_PREFIX))
        {
            //cerco nelle tint per prime in modo da fare cache anche dei colori
            TintAndColorWrapper createTintAndColorWrapperFromTint =
                    createTintAndColorWrapperFromTint(supertemplateDocument,
                            nameOrSelf);
            if (createTintAndColorWrapperFromTint != null)
            {
                return createTintAndColorWrapperFromTint;
            }

        } else if (nameOrSelf.startsWith(COLOR_PREFIX))
        {
            TintAndColorWrapper createTintAndColorWrapperFromColor =
                    createTintAndColorWrapperFromColor(supertemplateDocument,
                            nameOrSelf);
            if (createTintAndColorWrapperFromColor != null)
            {
                return createTintAndColorWrapperFromColor;
            }

        }
        //se sono qui vuol dire che la ricerca "ottimizzata" non ha dato risultati

        //cerco prima le tint che dovrebbero essere di meno e mi fanno fare cache dei colori nel caso debbano cercare il colore base
        TintAndColorWrapper createTintAndColorWrapperFromTint =
                createTintAndColorWrapperFromTint(supertemplateDocument,
                        nameOrSelf);
        if (createTintAndColorWrapperFromTint == null)
        {
            TintAndColorWrapper createTintAndColorWrapperFromColor =
                    createTintAndColorWrapperFromColor(supertemplateDocument,
                            nameOrSelf);
            return createTintAndColorWrapperFromColor;
        } else
        {
            return createTintAndColorWrapperFromTint;
        }

    }

    public static TintAndColorWrapper createTintAndColorWrapperFromColor(
            Document supertemplateDocument,
            String nameOrSelf)
            throws IdmlLibException
    {
        Color retrieveColorBySelf =
                retrieveColorBySelf(supertemplateDocument, nameOrSelf);
        if (retrieveColorBySelf != null)
        {
            TintAndColorWrapper wrapper = new TintAndColorWrapper(
                    retrieveColorBySelf);
            return wrapper;
        }
        return null;
    }

    public static TintAndColorWrapper createTintAndColorWrapperFromTint(
            Document supertemplateDocument,
            String nameOrSelf)
            throws IdmlLibException
    {
        Tint retrieveTintBySelf =
                retrieveTintBySelf(supertemplateDocument, nameOrSelf);
        if (retrieveTintBySelf != null)
        {
            Double tintValue = retrieveTintBySelf.getTintValue();
            String baseColor = retrieveTintBySelf.getBaseColor();
            Color retrieveColorBySelf =
                    retrieveColorBySelf(supertemplateDocument, baseColor);
            TintAndColorWrapper wrapper = new TintAndColorWrapper(
                    retrieveColorBySelf, tintValue);
            return wrapper;
        }
        return null;
    }

    public static int convertFromBase100ToBase255(Double fillTintValue)
    {
        double fromBase100ToValue = fillTintValue / 100.0;
        double fromValueToBase255 = fromBase100ToValue * 255.0;
        int integerBase255 = (int) fromValueToBase255;
        return integerBase255;
    }

    public static void cleanupAllCache()
    {
        cleanupCellStylesCache();
        cleanupObjectStylesCache();
        cleanupParagraphsStylesCache();
        cleanupTableStylesCache();
        cleanupColorsCache();
        cleanupTintsCache();
    }
}
