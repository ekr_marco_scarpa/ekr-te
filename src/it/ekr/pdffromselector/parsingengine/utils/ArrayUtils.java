/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.ekr.pdffromselector.parsingengine.utils;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Marco Scarpa
 */
public class ArrayUtils
{

    /**
     * confronta due list di stringhe per vedere se hanno lo stesso contenuto
     * @param a lista a
     * @param b lista b
     * @return true se a e b contengono le stesse stringhe
     */
    public static boolean areStringListsWithSameContent(List<String> a,
                                                        List<String> b)
    {
        if (a.size() == b.size())
        {
            for (int i = 0; i < a.size(); i++)
            {
                String fromA = a.get(i);
                String fromB = b.get(i);
                if (!fromA.equals(fromB))
                {
                    return false;
                }
            }
            //se dopo il for non ho trovato stringhe differenti, allora le liste sono uguali
            return true;
        } else
        {
            return false;
        }
    }
    
    public static float[] convertDoubleListToFloatArray(List<Double> list)
    {
        int size = list.size();
        float[] toReturn = new float[size];
        for (int i=0;i<size;i++)
        {
            Double get = list.get(i);
            if (get != null)
            {
                float floatValue = get.floatValue();
                toReturn[i] = floatValue;
            }
            //in java i vettori vengono inizializzati a 0, quindi non serve che gestisca se il valore è a null
        }
        return toReturn;
    }
    
    public static <E> void addToListIfNotAlreadyIn(List<E> list,E item)
    {
        for (E containedItem:list)
        {
            if (containedItem.equals(item))
            {
                return;
            } 
        }
        //se sono qui non ho trovatro oggetti uguali
        list.add(item);
    }
    
    /**
     * crea una array list con gli oggetti in posizione invertita rispetto alla lista originale
     * @param <E> il tipo della lista
     * @param list la lista da invertire
     * @return la lista invertita
     */
    public static <E> List<E> reverseList(List<E> list)
    {
        if (list != null)
        {
           if (list.size()>1)
           {
               //l'inverso di una stringa con un elemento è la stringa stessa per cui in quel caso non elaboro
               List<E> toReturn = new ArrayList<>();
               for (int i=list.size()-1;i>=0;i--)
               {
                   toReturn.add(list.get(i));
               }
               return toReturn;
           }
        }
        
        return list;
    }
}
