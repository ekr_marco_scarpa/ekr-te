/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.ekr.pdffromselector.parsingengine.utils;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author Marco Scarpa
 */
public class XmlManagement
{

    /**
     *
     * @param node
     * @param nodeName
     * @return
     */
    public static List<Node> retrieveNodesRecByName(Node node, String nodeName)
    {
        return retrieveNodesRecByNameWithOrWithoutStopping(node, nodeName, false,
                null, false);
    }

    /**
     *
     * @param node
     * @param nodeName
     * @return
     */
    public static List<Node> retrieveNodesRecByNameStoppingAtFirstLevelFound(
            Node node, String nodeName)
    {
        return retrieveNodesRecByNameWithOrWithoutStopping(node, nodeName, true,
                null, false);
    }

    /**
     *
     * @param node
     * @param nodeName
     * @param level
     * @return
     */
    public static List<Node> retrieveNodesOfACertainLevelByName(Node node,
                                                                String nodeName,
                                                                int level)
    {
        return retrieveNodesRecByNameWithOrWithoutStopping(node, nodeName, false,
                level, true);
    }

    /**
     *
     * @param node
     * @param nodeName
     * @param stoppingAtFirst
     * @param levelLimit
     * @param onlyAtLevelLimit
     * @return
     */
    public static List<Node> retrieveNodesRecByNameWithOrWithoutStopping(
            Node node, String nodeName, boolean stoppingAtFirst,
            Integer levelLimit, boolean onlyAtLevelLimit)
    {
        List<Node> toReturn = new ArrayList<>();
        if (node.getNodeName().equals(nodeName))
        {
            if (onlyAtLevelLimit && (levelLimit == 0))
            {
                //se voglio solo i nodi di un certo livello, metto il nodo e restituisco
                toReturn.add(node);
                return toReturn;
            } else
            {
                toReturn.add(node);
            }

            if (stoppingAtFirst)
            {
                //se sono arrivato al primo nodo con questo nome, lo restituisco subito
                return toReturn;
            }
        }
        if (levelLimit != null)
        {
            if (levelLimit == 0)
            {
                return toReturn;
            }
        }

        if (stoppingAtFirst)
        {
            List<Node> retrieveChildrenGivenName =
                    retrieveChildrenGivenName(node, nodeName);
            if (!retrieveChildrenGivenName.isEmpty())
            {
                return retrieveChildrenGivenName;
            }
        }
        NodeList childNodes = node.getChildNodes();
        for (int i = 0; i < childNodes.getLength(); i++)
        {
            Node childNode = childNodes.item(i);
            if (levelLimit != null)
            {
                //non decremento il valore di level limit direttamente perchè se no modificherei il valore a tutti i livelli di ricorsione e non è quello desiderato
                levelLimit = new Integer(levelLimit - 1);
            }
            toReturn.addAll(retrieveNodesRecByNameWithOrWithoutStopping(
                    childNode, nodeName, stoppingAtFirst, levelLimit,
                    onlyAtLevelLimit));

        }
        return toReturn;
    }

    /**
     *
     * @param parent
     * @param searchingName
     * @return
     */
    public static List<Node> retrieveChildrenGivenName(Node parent,
                                                       String searchingName)
    {
        List<Node> toReturn = new ArrayList<>();
        NodeList childNodes = parent.getChildNodes();
        for (int i = 0; i < childNodes.getLength(); i++)
        {
            Node childNode = childNodes.item(i);
            if (childNode.getNodeName().equals(searchingName))
            {
                toReturn.add(childNode);
            }
        }
        return toReturn;
    }

    /**
     *
     * @param string
     * @return
     */
    public static boolean convertStringToBoolean(String string)
    {
        return string.equalsIgnoreCase("true");
    }

    /**
     *
     * @param examining
     * @return
     */
    public static boolean hasOnlyTextContent(Node examining)
    {
        if (!examining.hasChildNodes())
        {
            //se non ha figli assumo cheabbia contenuto testuale vuoto
            return true;
        } else
        {
            NodeList childNodes = examining.getChildNodes();
            for (int i = 0; i < childNodes.getLength(); i++)
            {
                Node childNode = childNodes.item(i);
                if (childNode.getNodeType() != Node.TEXT_NODE)
                {
                    return false;
                }
            }
            //se dopo aver visto tutti i figli non sono text node, allora per me vanno bene
            return true;
        }
    }

    /**
     *
     * @param parent
     * @return
     */
    public static List<Node> retrieveElementChildren(Node parent)
    {
        NodeList childNodes = parent.getChildNodes();
        List<Node> toReturn = new ArrayList<>();
        for (int i = 0; i < childNodes.getLength(); i++)
        {
            Node childNode = childNodes.item(i);
            if (childNode.getNodeType() == Node.ELEMENT_NODE)
            {
                toReturn.add(childNode);
            }
        }
        return toReturn;
    }

    /**
     *
     * @param parent
     * @return
     */
    public static List<Node> retrieveLeavesNodes(Node parent)
    {
        List<Node> toReturn = new ArrayList<>();
        NodeList childNodes = parent.getChildNodes();
        if (childNodes.getLength() == 0)
        {
            //il nodo in questione è un tag senza figli, quindi foglia
            toReturn.add(parent);
            return toReturn;
        }
        if (childNodes.getLength() == 1)
        {
            //vedo se il figlio è un nodo di testo, in tal caso sono in un nodo foglia
            Node item = childNodes.item(0);
            if (item.getNodeType() == Node.TEXT_NODE)
            {
                toReturn.add(parent);
                return toReturn;
            }
        }else
        {
            //verifico se tutti i figli di questo nodo sono nodi testo o nodi commento
            if (areAllChildrenNodesTextOrComments(parent))
            {
                toReturn.add(parent);
                return toReturn;
            }
           
        }
        //se sono qui vuol dire che il nodo parent non è foglia, quindi ricerco ricorsivamente, nei suoi elementi
        List<Node> elementChildren = retrieveElementChildren(parent);
        for (Node elementChild : elementChildren)
        {
            List<Node> childLeavesNodes = retrieveLeavesNodes(elementChild);
            if (childLeavesNodes != null)
            {
                toReturn.addAll(childLeavesNodes);
            }

        }

        return toReturn;
    }
    
    public static boolean areAllChildrenNodesTextOrComments(Node parent)
    {
        NodeList childNodes = parent.getChildNodes();
        for (int i=0;i<childNodes.getLength();i++)
        {
            Node item = childNodes.item(i);
            if (item.getNodeType() != Node.TEXT_NODE)
            {
                if(item.getNodeType() != Node.COMMENT_NODE)
                {
                    return false;
                }
            }
        }
        //se sono qui vuol dire che i nodi figli sono tutti testi o commenti
        return true;
    }

    /**
     *
     * @param parent
     * @return
     */
    public static Node retrieveLeafNode(Node parent)
    {
        List<Node> retrieveLeavesNodes = retrieveLeavesNodes(parent);
        if (retrieveLeavesNodes.size() == 1)
        {
            //se ho un solo nodo foglia lo restituisco
            Node singleLeaf = retrieveLeavesNodes.get(0);
            return singleLeaf;
        }
        return null;
    }

    public static String nodeToString(Node node)
    {
        StringWriter sw = new StringWriter();
        try
        {
            Transformer t = TransformerFactory.newInstance().newTransformer();
            t.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            t.transform(new DOMSource(node), new StreamResult(sw));
        } catch (TransformerException te)
        {
            System.out.println("nodeToString Transformer Exception");
        }
        return sw.toString();
    }
    
    public static boolean checkIfNodeIsAncestor(Node candidateAncestor,Node currentNode)
    {
        Node currentAncestor = currentNode.getParentNode();
        while (currentAncestor != null)
        {
            if (currentAncestor.equals(candidateAncestor))
            {
                return true;
            }
            currentAncestor = currentAncestor.getParentNode();
        }
        
        return false;
    }
}
