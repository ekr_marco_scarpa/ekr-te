/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.ekr.pdffromselector.parsingengine.utils;

import com.itextpdf.text.pdf.PdfPCell;

/**
 *
 * @author Marco Scarpa
 */
public class ReachedCellRectangleAndImageIndexWrapper
{
    public PdfPCell reachedCell;
    public int imageIndex = 1;
    public boolean stopIterating = true;
    public SelectorLayoutCellAndRectangleWrapper cellAndRectangleWrapper = null;
    
}
