package it.ekr.pdffromselector.parsingengine.exceptions.pdfexceptions;

public class ImageBackgroundException extends AbstractPdfException 
{
  public ImageBackgroundException()
  {
	  super("errore sull'immagine di sfondo del supertemplate");
  }
  
  public ImageBackgroundException(Throwable cause)
  {
	  super("errore sull'immagine di sfondo del supertemplate "+cause.getLocalizedMessage(),cause);
  }
}
