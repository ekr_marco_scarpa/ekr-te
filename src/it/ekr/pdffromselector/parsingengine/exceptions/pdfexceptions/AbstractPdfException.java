/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.ekr.pdffromselector.parsingengine.exceptions.pdfexceptions;

/**
 *
 * @author Marco Scarpa
 */
public abstract class AbstractPdfException extends Exception
{

    public AbstractPdfException()
    {
    }

    public AbstractPdfException(String message)
    {
        super(message);
    }

    public AbstractPdfException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public AbstractPdfException(Throwable cause)
    {
        super(cause);
    }

    
    
}
