/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.ekr.pdffromselector.parsingengine.exceptions.pdfexceptions;

import it.ekr.pdffromselector.parsingengine.utils.*;

/**
 *
 * @author Marco Scarpa
 */
public class TrialsLimitException extends AbstractPdfException
{

    public TrialsLimitException(SelectorLayoutCellAndTrialsWrapper trialsAndCell)
    {
       super("raggiunto limite di tentativi di impaginazione subpage "+trialsAndCell.getCurrentTrial()+"/"+trialsAndCell.getTrialLimit());
    }
    
}
