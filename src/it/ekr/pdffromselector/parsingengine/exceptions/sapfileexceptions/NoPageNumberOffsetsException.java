/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.ekr.pdffromselector.parsingengine.exceptions.sapfileexceptions;

/**
 *
 * @author Marco Scarpa
 */
public class NoPageNumberOffsetsException extends SapFileException
{

    /**
	 * 
	 */
	private static final long serialVersionUID = -3097469375534229081L;

	public NoPageNumberOffsetsException(String message)
    {
        super(message);
    }
    
}
