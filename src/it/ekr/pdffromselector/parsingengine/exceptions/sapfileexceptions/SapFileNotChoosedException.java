/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.ekr.pdffromselector.parsingengine.exceptions.sapfileexceptions;

/**
 *
 * @author Marco Scarpa
 */
public class SapFileNotChoosedException extends SapFileException
{

    /**
	 * 
	 */
	private static final long serialVersionUID = -3976350490757699540L;

	/**
     *
     * @param message
     */
    public SapFileNotChoosedException(String message)
    {
        super(message);
    }

}
