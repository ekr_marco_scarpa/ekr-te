/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.ekr.pdffromselector.parsingengine.exceptions.sapfileexceptions;

/**
 *
 * @author Marco Scarpa
 */
public class SapFileException extends Exception
{

    /**
	 * 
	 */
	private static final long serialVersionUID = -6763315748318579151L;

	/**
     *
     * @param message
     */
    public SapFileException(String message)
    {
        super(message);
    }

}
