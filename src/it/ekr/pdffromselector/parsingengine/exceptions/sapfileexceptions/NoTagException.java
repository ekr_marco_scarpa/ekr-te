/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.ekr.pdffromselector.parsingengine.exceptions.sapfileexceptions;

import it.ekr.pdffromselector.parsingengine.ParsingSapXml;

/**
 *
 * @author Marco Scarpa
 */
public class NoTagException extends SapFileException
{

    public NoTagException(String tag)
    {
        super("not found any tag "+tag);
    }
    
}
