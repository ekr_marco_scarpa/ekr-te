/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.ekr.pdffromselector.parsingengine.exceptions.selectordocumentexceptions;

/**
 *
 * @author Marco Scarpa
 */
public class SelectorFileNotExistentException extends SelectorDocumentException
{

    /**
     *
     * @param message
     */
    public SelectorFileNotExistentException(String message)
    {
        super(message);
    }

}
