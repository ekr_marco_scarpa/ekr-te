/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.ekr.pdffromselector.parsingengine.selectorobjects;

import de.fhcon.idmllib.api.IdmlLibException;
import it.ekr.pdffromselector.parsingengine.exceptions.selectordocumentexceptions.SelectorDocumentException;
import java.io.IOException;
import java.net.URISyntaxException;
import javax.xml.parsers.*;
import org.xml.sax.*;

/**
 *
 * @author Marco Scarpa
 */
public interface ISelectorContainer extends ISelectorObject
{

    /**
     * Fornisce l'altezza interna disponibile
     *
     * @return l'altezza interna disponibile
     * @throws URISyntaxException
     * @throws IOException
     * @throws IdmlLibException
     * @throws SelectorDocumentException
     */
    double getAvailableHeight() throws URISyntaxException, IOException,
            IdmlLibException, SelectorDocumentException;

    /**
     * Fornisce la larghezza interna disponibile
     *
     * @return la larghezza interna disponibile
     * @throws URISyntaxException
     * @throws IOException
     * @throws IdmlLibException
     * @throws SelectorDocumentException
     */
    double getAvailableWidth() throws URISyntaxException, IOException,
            IdmlLibException, SelectorDocumentException;

    /**
     * l'altezza dell'oggetto corrente
     *
     * @return l'altezza dell'oggetto corrente
     * @throws URISyntaxException
     * @throws IOException
     * @throws IdmlLibException
     * @throws SelectorDocumentException
     */
    double getHeight() throws URISyntaxException, IOException, IdmlLibException,
            SelectorDocumentException;

    /**
     * la larghezza dell'oggetto corrente
     *
     * @return la larghezza dell'oggetto corrente
     * @throws URISyntaxException
     * @throws IOException
     * @throws IdmlLibException
     * @throws SelectorDocumentException
     */
    public double getWidth() throws URISyntaxException, IOException,
            IdmlLibException, SelectorDocumentException,SAXException, ParserConfigurationException;

    /**
     *
     * @return
     */
    public ISelectorContainer getAncestorContainer();
}
