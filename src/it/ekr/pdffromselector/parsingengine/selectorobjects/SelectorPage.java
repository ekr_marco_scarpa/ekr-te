/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.ekr.pdffromselector.parsingengine.selectorobjects;

import de.fhcon.idmllib.api.IdmlLibException;
import de.fhcon.idmllib.api.elements.masterspread.MasterSpread;
import de.fhcon.idmllib.api.elements.shared.*;
import it.ekr.pdffromselector.parsingengine.exceptions.selectordocumentexceptions.SelectorDocumentException;
import it.ekr.pdffromselector.parsingengine.exceptions.selectordocumentexceptions.SelectorFileNotExistentException;
import it.ekr.pdffromselector.parsingengine.utils.*;
import java.io.File;
import java.io.IOException;
import java.net.*;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author Marco Scarpa
 */
public class SelectorPage extends AbstractSelectorContainer implements
        ISelectorContainer
{

    private String inheritedMastro = null;
    private Node pageReferenceXML = null;
    private SelectorFascicle parentFascicle = null;

    private String mastro;

    private double height = 0;

    private double width = 0;

    private double availableWidth = 0;
    private double availableHeight = 0;
    Page masterPage = null;
    private Double topMargin = null;
    private Double bottomMargin = null;
    private Double leftMargin = null;
    private Double rightMargin = null;
    private MasterSpread masterSpread = null;
    private List<File> imagesInMastroFirstLevel = null;

    /**
     *
     * @param pageFile
     * @param inherithedMastro
     * @param pageReferenceXML
     * @param parentFascicle
     * @throws SelectorFileNotExistentException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     */
    public SelectorPage(File pageFile, String inherithedMastro,
                        Node pageReferenceXML, SelectorFascicle parentFascicle)
            throws SelectorFileNotExistentException,
            ParserConfigurationException, SAXException, IOException
    {
        super(parentFascicle, null);
        this.inheritedMastro = inherithedMastro;
        this.pageReferenceXML = pageReferenceXML;
        this.parentFascicle = parentFascicle;
        if (pageFile.exists())
        {
            DocumentBuilderFactory newInstance =
                    DocumentBuilderFactory.newInstance();
            DocumentBuilder newDocumentBuilder = newInstance.
                    newDocumentBuilder();
            Document parsedPage = newDocumentBuilder.parse(pageFile);
            NodeList childNodes = parsedPage.getChildNodes();
            //per la pagina xmlDescription è l'xml della pagina
            xmlDescription = childNodes.item(0);
        } else
        {
            throw new SelectorFileNotExistentException(
                    "Non è stato trovato il file della pagina " + pageFile.
                    getAbsolutePath());
        }
    }

    /**
     * indica il nome della mastro usata in questa pagina
     *
     * @return the value of mastro
     */
    public String getMastro()
    {
        if (mastro == null)
        {
            mastro = calculateMastro();
        }
        return mastro;
    }

    private String calculateMastro()
    {
        NamedNodeMap attributes = xmlDescription.getAttributes();
        Node riferimentoMastroAttribute = attributes.getNamedItem(
                "riferimento_mastro");
        if (riferimentoMastroAttribute != null)
        {
            return riferimentoMastroAttribute.getNodeValue();
        }
        return inheritedMastro;
    }

    @Override
    protected List<ISelectorObject> calculateContainedObjects()
            throws SelectorDocumentException, SelectorFileNotExistentException,
            ParserConfigurationException, SAXException, IOException
    {
        //recupero la layout cell principale
        List<Node> childNodes = XmlManagement.retrieveElementChildren(
                xmlDescription);
        for (int i = 0; i < childNodes.size(); i++)
        {
            Node candidateMainCellXML = childNodes.get(i);
            if (candidateMainCellXML.getNodeName().equals(
                    SelectorLayoutCell.LAYOUT_CELL_TAG))
            {
                //la pagina come figlia ha solo la layout cell e quindi la restituisco subito
                List<ISelectorObject> wrapper = new ArrayList<>();
                SelectorLayoutCell mainCell;
                mainCell =
                        new SelectorLayoutCell(candidateMainCellXML, this);
                wrapper.add(mainCell);
                return wrapper;
            }
        }

        return null;
    }

    @Override
    public double getHeight() throws URISyntaxException, IOException,
            IdmlLibException, SelectorDocumentException
    {
        if (height == 0)
        {
            height = calculateHeight();
        }
        return height;
    }

    @Override
    public double getAvailableWidth() throws
            URISyntaxException, IOException, IdmlLibException,
            SelectorDocumentException
    {
        if (availableWidth == 0)
        {
            //anche se fosse 0, vorrebbe dire che ogni volta verrebbe ricalcolata ma è accettabile perchè è un caso estremo
            availableWidth = calculateAvailableWidth();
        }

        return availableWidth;

    }

    private double calculateAvailableWidth() throws URISyntaxException,
            IOException, IdmlLibException, SelectorDocumentException
    {
        return calculateAvailableWidthOrHeight(true);
    }

    private double calculateAvailableHeight() throws URISyntaxException,
            IOException, IdmlLibException, SelectorDocumentException
    {
        return calculateAvailableWidthOrHeight(false);
    }

    private double calculateAvailableWidthOrHeight(
            boolean calculateWidth) throws URISyntaxException, IOException,
            IdmlLibException, SelectorDocumentException
    {

        Page currentMasterPage = retrieveDesiredMasterPage();
        if (currentMasterPage != null)
        {
            Double calculateDimensionFromGeometricBounds =
                    calculateWidthOrHeight(calculateWidth);
            if (calculateDimensionFromGeometricBounds != 0d)
            {
                MarginPreference marginPreference =
                        currentMasterPage.getMarginPreference();
                Double leftOrTop;
                Double rightOrBottom;
                if (calculateWidth)
                {
                    leftOrTop = marginPreference.getLeft();
                    rightOrBottom = marginPreference.getRight();

                } else
                {
                    leftOrTop = marginPreference.getTop();
                    rightOrBottom = marginPreference.getBottom();

                }
                return subtractMarginsFromFullLength(
                        calculateDimensionFromGeometricBounds,
                        leftOrTop, rightOrBottom);
            }

        }
        return 0;

    }

    private MasterSpread getMasterSpread() throws URISyntaxException,
            IdmlLibException, IOException, SelectorDocumentException
    {
        if (masterSpread == null)
        {
            masterSpread = retrieveMasterSpread();
        }
        return masterSpread;

    }

    private MasterSpread retrieveMasterSpread() throws URISyntaxException,
            IdmlLibException, IOException, SelectorDocumentException
    {
        SelectorDocument currentParentSelectorDocument =
                getParentSelectorDocument();
        de.fhcon.idmllib.api.elements.Document supertemplateDocument =
                currentParentSelectorDocument.getSupertemplateDocument();

        List<String> masterSpreadIdList =
                supertemplateDocument.getMasterSpreadIdList();
        for (String masterSpreadId : masterSpreadIdList)
        {
            MasterSpread masterSpreadById =
                    supertemplateDocument.getMasterSpreadById(masterSpreadId);
            String masterSpreadName = masterSpreadById.getName();
            String desiredMastro = getMastro();
            if (masterSpreadName.equals(desiredMastro))
            {
                return masterSpreadById;
            }

        }
        return null;
    }

    private Page retrieveDesiredMasterPage() throws URISyntaxException,
            IOException, IdmlLibException, SelectorDocumentException
    {
        if (masterPage != null)
        {
            return masterPage;//caching pagina mastro
        }

        //devo recuperare la master spread in cui ho la mastro
        MasterSpread masterSpreadById = getMasterSpread();
        if (masterSpreadById != null)
        {
            List<Page> pageList = masterSpreadById.getPageList();
            //ad ora assumo che la mastro sia a pagina unica
            masterPage = pageList.get(0);
            return masterPage;
        }

        return null;
    }

    /**
     * cerca le immagini nei rectangle delle mastro, non fa ricerca ricorsiva
     *
     * @return le immagini presenti nei rectangle di primo livello delle mastro
     */
    public List<File> getImagesInMastroFirstLevel() throws URISyntaxException,
            IOException, IdmlLibException, SelectorDocumentException
    {
        if (imagesInMastroFirstLevel == null)
        {
            imagesInMastroFirstLevel = calculateImagesInMastroFirstLevel();
        }
        return imagesInMastroFirstLevel;
    }

    protected List<File> calculateImagesInMastroFirstLevel() throws
            IdmlLibException, SelectorDocumentException, URISyntaxException,
            IOException
    {
        List<File> toReturn = new ArrayList<>();
        MasterSpread currentMasterSpread = getMasterSpread();
        if (currentMasterSpread != null)
        {
            List<Rectangle> rectangleList = currentMasterSpread.
                    getRectangleList();
            for (Rectangle rect : rectangleList)
            {
                List<Link> linkList = rect.getLinkList();
                for (Link link : linkList)
                {
                    String linkResourceURI = link.getLinkResourceURI();
                    linkResourceURI =
                                normalizeFilePartOfUri(linkResourceURI);
                    File toAddFile = new File(linkResourceURI);
                    toReturn.add(toAddFile);
                }
                List<PDF> pdfList = rect.getPDFList();
                for (PDF pdf : pdfList)
                {
                    List<Link> linkListPdf = pdf.getLinkList();
                    for (Link link : linkListPdf)
                    {
                        String linkResourceURI = link.getLinkResourceURI();
                        linkResourceURI =
                                normalizeFilePartOfUri(linkResourceURI);
                        File toAddFile = new File(linkResourceURI);
                        toReturn.add(toAddFile);
                    }
                }
                List<PICT> pictList = rect.getPICTList();
                for (PICT pict:pictList)
                {
                    List<Link> linkListPict = pict.getLinkList();
                    for (Link link : linkListPict)
                    {
                        String linkResourceURI = link.getLinkResourceURI();
                        linkResourceURI =
                                normalizeFilePartOfUri(linkResourceURI);
                        File toAddFile = new File(linkResourceURI);
                        toReturn.add(toAddFile);
                    }
                }
            }
        }
        return toReturn;
    }

    protected String normalizeFilePartOfUri(String linkResourceURI)
    {
        if (!linkResourceURI.startsWith("file://")) {
            if (linkResourceURI.startsWith("file:")) {
                linkResourceURI =
                        linkResourceURI.replace("file:", "");
            }
        }
        return linkResourceURI;
    }


    /**
     * Get the value of topMargin
     *
     * @return the value of topMargin
     * @throws java.net.URISyntaxException
     * @throws java.io.IOException
     * @throws de.fhcon.idmllib.api.IdmlLibException
     * @throws
     * it.ekr.pdffromselector.parsingengine.exceptions.selectordocumentexceptions.SelectorDocumentException
     */
    public Double getTopMargin() throws URISyntaxException, IOException,
            IdmlLibException, SelectorDocumentException
    {
        if (topMargin == null)
        {
            topMargin = calculateTopMargin();
        }
        return topMargin;
    }

    private Double calculateTopMargin() throws URISyntaxException, IOException,
            IdmlLibException, SelectorDocumentException
    {
        Page currentMasterPage = retrieveDesiredMasterPage();
        if (currentMasterPage != null)
        {
            MarginPreference marginPreference =
                    currentMasterPage.getMarginPreference();
            Double margin = marginPreference.getTop();
            return margin;

        }
        return null;
    }

    /**
     * Get the value of bottomMargin
     *
     * @return the value of bottomMargin
     * @throws java.net.URISyntaxException
     * @throws java.io.IOException
     * @throws de.fhcon.idmllib.api.IdmlLibException
     * @throws
     * it.ekr.pdffromselector.parsingengine.exceptions.selectordocumentexceptions.SelectorDocumentException
     */
    public Double getBottomMargin() throws URISyntaxException, IOException,
            IdmlLibException, SelectorDocumentException
    {
        if (bottomMargin == null)
        {
            bottomMargin = calculateBottomMargin();
        }
        return bottomMargin;
    }

    private Double calculateBottomMargin() throws URISyntaxException,
            IOException, IdmlLibException, SelectorDocumentException
    {
        Page currentMasterPage = retrieveDesiredMasterPage();
        if (currentMasterPage != null)
        {
            MarginPreference marginPreference =
                    currentMasterPage.getMarginPreference();
            Double margin = marginPreference.getBottom();
            return margin;

        }
        return null;
    }

    /**
     * Get the value of leftMargin
     *
     * @return the value of leftMargin
     * @throws java.net.URISyntaxException
     * @throws java.io.IOException
     * @throws de.fhcon.idmllib.api.IdmlLibException
     * @throws
     * it.ekr.pdffromselector.parsingengine.exceptions.selectordocumentexceptions.SelectorDocumentException
     */
    public Double getLeftMargin() throws URISyntaxException, IOException,
            IdmlLibException, SelectorDocumentException
    {
        if (leftMargin == null)
        {
            leftMargin = calculateLeftMargin();
        }
        return leftMargin;
    }

    private Double calculateLeftMargin() throws URISyntaxException, IOException,
            IdmlLibException, SelectorDocumentException
    {
        Page currentMasterPage = retrieveDesiredMasterPage();
        if (currentMasterPage != null)
        {
            MarginPreference marginPreference =
                    currentMasterPage.getMarginPreference();
            Double margin = marginPreference.getLeft();
            return margin;

        }
        return null;
    }

    /**
     * Get the value of rightMargin
     *
     * @return the value of rightMargin
     * @throws java.net.URISyntaxException
     * @throws java.io.IOException
     * @throws de.fhcon.idmllib.api.IdmlLibException
     * @throws
     * it.ekr.pdffromselector.parsingengine.exceptions.selectordocumentexceptions.SelectorDocumentException
     */
    public Double getRightMargin() throws URISyntaxException, IOException,
            IdmlLibException, SelectorDocumentException
    {
        if (rightMargin == null)
        {
            rightMargin = calculateRightMargin();
        }
        return rightMargin;
    }

    private Double calculateRightMargin() throws URISyntaxException, IOException,
            IdmlLibException, SelectorDocumentException
    {
        Page currentMasterPage = retrieveDesiredMasterPage();
        if (currentMasterPage != null)
        {
            MarginPreference marginPreference =
                    currentMasterPage.getMarginPreference();
            Double margin = marginPreference.getRight();
            return margin;

        }
        return null;
    }

    private double calculateWidthOrHeight(boolean calculateWidth) throws
            URISyntaxException, IOException, IdmlLibException,
            SelectorDocumentException
    {
        Page currentMasterPage = retrieveDesiredMasterPage();
        if (currentMasterPage != null)
        {
            Node elementNode = currentMasterPage.getElementNode();
            NamedNodeMap attributes = elementNode.getAttributes();
            Node geometricBounds = attributes.
                    getNamedItem("GeometricBounds");
            Double calculateDimensionFromGeometricBounds = null;
            if (geometricBounds != null)
            {
                String geometricBoundsValue = geometricBounds.
                        getTextContent();
                if (calculateWidth)
                {
                    calculateDimensionFromGeometricBounds =
                            IdmlUtils.calculateWidthFromGeometricBounds(
                                    geometricBoundsValue);
                } else
                {
                    calculateDimensionFromGeometricBounds =
                            IdmlUtils.calculateHeightFromGeometricBounds(
                                    geometricBoundsValue);
                }

            }
            if (calculateDimensionFromGeometricBounds != null)
            {
                return calculateDimensionFromGeometricBounds;
            }

        }

        return 0;
    }

    private double calculateHeight() throws URISyntaxException, IOException,
            IdmlLibException, SelectorDocumentException
    {
        return calculateWidthOrHeight(false);
    }

    private double calculateWidth() throws URISyntaxException,
            IOException, IdmlLibException,
            SelectorDocumentException
    {
        return calculateWidthOrHeight(true);
    }

    private double subtractMarginsFromFullLength(Double fullLength,
                                                 Double leftOrTop,
                                                 Double rightOrBottom)
    {
        Double acc = fullLength;
        if (acc != null)
        {
            if (leftOrTop != null)
            {
                acc -= leftOrTop;
            }
            if (rightOrBottom != null)
            {
                acc -= rightOrBottom;
            }

            return acc;
        } else
        {
            return 0;
        }
    }

    @Override
    public double getAvailableHeight() throws URISyntaxException,
            IOException, IdmlLibException,
            SelectorDocumentException
    {
        if (availableHeight == 0)
        {
            availableHeight = calculateAvailableHeight();
        }
        return availableHeight;
    }

    @Override
    public double getWidth() throws URISyntaxException, IOException,
            IdmlLibException,
            SelectorDocumentException
    {
        if (width == 0)
        {
            width = calculateWidth();
        }
        return width;
    }

    @Override
    public ISelectorObject getParent()
    {
        return parentFascicle;
    }

    @Override
    public SelectorPage getParentPage()
    {
        return this;
    }

    @Override
    public void cleanupCachedData()
    {
        super.cleanupCachedData(); //To change body of generated methods, choose Tools | Templates.
        availableHeight = 0;
        availableWidth = 0;
        bottomMargin = null;
        height = 0;
        imagesInMastroFirstLevel = null;
        leftMargin = null;
        masterPage = null;
        masterSpread = null;
        mastro = null;
        rightMargin = null;
        topMargin = null;
        width = 0;
    }

    
}
