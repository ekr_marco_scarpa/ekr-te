package it.ekr.pdffromselector.parsingengine.selectorobjects;

import de.fhcon.idmllib.api.IdmlLibException;
import de.fhcon.idmllib.api.elements.Document;
import de.fhcon.idmllib.api.elements.styles.ObjectStyle;
import it.ekr.pdffromselector.parsingengine.exceptions.selectordocumentexceptions.SelectorDocumentException;
import it.ekr.pdffromselector.parsingengine.exceptions.selectordocumentexceptions.SelectorFileNotExistentException;
import it.ekr.pdffromselector.parsingengine.utils.IdmlUtils;
import it.ekr.pdffromselector.parsingengine.utils.TintAndColorWrapper;
import it.ekr.pdffromselector.parsingengine.utils.XmlManagement;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

/**
 *
 * @author Marco Scarpa
 */
public class SelectorSpaceUnit extends AbstractSelectorUnit implements
        ISelectorContainer
{

    public static final String SPACE_UNIT_TAG = "spaziale";
    private Boolean enableStroke = null;

    private SelectorSpaceImage containedImage;
    private ObjectStyle objectStyle;
    protected List<ObjectStyle> objectStylesChain = null;
    private Double strokeWeight = null;
    private String strokeColor = null;
    private Double strokeTint = null;
    private TintAndColorWrapper strokeTintAndColor = null;

    /**
     *
     * @param parent
     * @param unitXml
     */
    public SelectorSpaceUnit(ISelectorObject parent, Node unitXml)
    {
        super(parent, unitXml);
    }

    /**
     * Get the value of containedImage
     *
     * @return the value of containedImage
     */
    public SelectorSpaceImage getContainedImage()
    {
        if (containedImage == null)
        {
            containedImage = calculateContainedImage();
        }
        return containedImage;
    }

    private SelectorSpaceImage calculateContainedImage()
    {
        //assumo che vi sia solo un box ancorato immagine
        List<Node> boxAncoratoImmagineList =
                XmlManagement.retrieveNodesRecByNameStoppingAtFirstLevelFound(
                        xmlDescription, "box_ancorato_immagine");
        Node boxAncoratoImmagineTag = boxAncoratoImmagineList.get(0);
        SelectorSpaceImage selectorSpaceImage =
                new SelectorSpaceImage(this, boxAncoratoImmagineTag,
                        getTypePath(null));
        return selectorSpaceImage;
    }

    @Override
    public List<ISelectorObject> calculateContainedObjects() throws
            SelectorDocumentException, SelectorFileNotExistentException,
            ParserConfigurationException, SAXException, IOException
    {
        //assumo che l'oggetto space contenga solo un'immagine
        List<ISelectorObject> wrapper = new ArrayList<>();
        wrapper.add(getContainedImage());
        return wrapper;
    }

    @Override
    public double getAvailableHeight() throws URISyntaxException, IOException,
            IdmlLibException, SelectorDocumentException
    {
        Double currentStrokeWeight = getStrokeWeight();
        if (currentStrokeWeight != null)
        {
            return getHeight() - 2 * currentStrokeWeight;
        }
        return getHeight();
    }

    @Override
    public double getAvailableWidth() throws URISyntaxException, IOException,
            IdmlLibException, SelectorDocumentException
    {
        Double currentStrokeWeight = getStrokeWeight();
        if (currentStrokeWeight != null)
        {
            return getWidth() - 2 * currentStrokeWeight;
        }
        return getWidth();
    }

    @Override
    public double getHeight() throws URISyntaxException, IOException,
            IdmlLibException, SelectorDocumentException
    {
        //la dimensione è decisa dall'immagine, la dimensione totale dell'oggetto space è quella del contenitore
        ISelectorContainer ancestorContainer = getAncestorContainer();
        return ancestorContainer.getAvailableHeight();
    }

    @Override
    public double getWidth() throws URISyntaxException, IOException,
            IdmlLibException, SelectorDocumentException
    {
        //la dimensione è decisa dall'immagine, la dimensione totale dell'oggetto space è quella del contenitore
        ISelectorContainer ancestorContainer = getAncestorContainer();
        return ancestorContainer.getAvailableWidth();
    }

    @Override
    public ISelectorContainer getAncestorContainer()
    {
        ISelectorObject currentAncestor = getParent();
        while (currentAncestor != null)
        {
            if (currentAncestor instanceof ISelectorContainer)
            {
                return (ISelectorContainer) currentAncestor;
            }
            currentAncestor = currentAncestor.getParent();
        }

        return null;
    }

    /**
     * Get the value of objectStyle
     *
     * @return the value of objectStyle
     * @throws java.net.URISyntaxException
     * @throws java.io.IOException
     * @throws de.fhcon.idmllib.api.IdmlLibException
     * @throws
     * it.ekr.pdffromselector.parsingengine.exceptions.selectordocumentexceptions.SelectorDocumentException
     */
    public ObjectStyle getObjectStyle() throws URISyntaxException, IOException,
            IdmlLibException, SelectorDocumentException
    {
        if (objectStyle == null)
        {
            objectStyle = calculateObjectStyle();
        }
        return objectStyle;
    }

    /**
     *
     * @return @throws URISyntaxException
     * @throws IOException
     * @throws IdmlLibException
     * @throws SelectorDocumentException
     */
    protected ObjectStyle calculateObjectStyle() throws URISyntaxException,
            IOException, IdmlLibException, SelectorDocumentException
    {
        SelectorDocument currentParentSelectorDocument =
                this.getParentSelectorDocument();
        Document supertemplateDocument =
                currentParentSelectorDocument.getSupertemplateDocument();

        String objectStyleName =
                IdmlUtils.convertTypePathToIdmlName(getTypePath(null));
        ObjectStyle found = IdmlUtils.retrieveObjectStyleByName(
                supertemplateDocument, objectStyleName);
        return found;

    }

    /**
     * trova lo stile di oggetto di questo oggetto e quelli in cui si basa, in
     * modo da poter cercare le proprietà grafiche
     *
     * @return the value of paragraphStylesChain
     * @throws java.net.URISyntaxException
     * @throws de.fhcon.idmllib.api.IdmlLibException
     * @throws java.io.IOException
     * @throws
     * it.ekr.pdffromselector.parsingengine.exceptions.selectordocumentexceptions.SelectorDocumentException
     */
    protected List<ObjectStyle> getObjectStylesChain() throws
            URISyntaxException, IdmlLibException, IOException,
            SelectorDocumentException, URISyntaxException
    {
        if (objectStylesChain == null)
        {
            objectStylesChain = calculateObjectStylesChain();
        }
        return Collections.unmodifiableList(objectStylesChain);
    }

    /**
     *
     * @return @throws URISyntaxException
     * @throws IdmlLibException
     * @throws IOException
     * @throws SelectorDocumentException
     */
    protected List<ObjectStyle> calculateObjectStylesChain() throws
            URISyntaxException, IdmlLibException, IOException,
            SelectorDocumentException
    {
        return IdmlUtils.retrieveObjectStylesCurrentAndBasedOnChain(
                getParentSelectorDocument().getSupertemplateDocument(),
                getObjectStyle());
    }

    /**
     * Get the value of strokeWeight
     *
     * @return the value of strokeWeight
     * @throws java.net.URISyntaxException
     * @throws de.fhcon.idmllib.api.IdmlLibException
     * @throws java.io.IOException
     * @throws
     * it.ekr.pdffromselector.parsingengine.exceptions.selectordocumentexceptions.SelectorDocumentException
     */
    public Double getStrokeWeight() throws URISyntaxException, IdmlLibException,
            IOException, SelectorDocumentException
    {
        if (strokeWeight == null)
        {
            strokeWeight = calculateStrokeWeight();
        }
        return strokeWeight;
    }

    /**
     *
     * @return @throws URISyntaxException
     * @throws IdmlLibException
     * @throws IOException
     * @throws SelectorDocumentException
     */
    private Double calculateStrokeWeight() throws URISyntaxException,
            IdmlLibException, IOException, SelectorDocumentException
    {
        if (isEnableStroke() == null || !isEnableStroke())
        {
            return null;
        }
        List<ObjectStyle> stylesChain = getObjectStylesChain();
        for (ObjectStyle style : stylesChain)
        {
            Double candidateStrokeWeight = style.getStrokeWeight();
            if (candidateStrokeWeight != null)
            {
                return candidateStrokeWeight;
            }
        }
        return null;
    }

    public Boolean isEnableStroke() throws URISyntaxException, IdmlLibException,
            IOException, SelectorDocumentException
    {
        if (enableStroke == null)
        {
            enableStroke = calculateEnableStroke();
        }
        return enableStroke;
    }

    private Boolean calculateEnableStroke() throws URISyntaxException,
            IdmlLibException, IOException, SelectorDocumentException
    {
        List<ObjectStyle> stylesChain = getObjectStylesChain();
        for (ObjectStyle style : stylesChain)
        {
            Boolean candidateEnable = style.isEnableStroke();
            if (candidateEnable != null)
            {
                return candidateEnable;
            }
        }
        return null;
    }

    public String getStrokeColor() throws URISyntaxException, IdmlLibException,
            IOException, SelectorDocumentException
    {
        if (strokeColor == null)
        {
            strokeColor = calculateStrokeColor();
        }
        return strokeColor;
    }

    private String calculateStrokeColor() throws URISyntaxException,
            IdmlLibException, IOException, SelectorDocumentException
    {
        if (!isEnableStroke())
        {
            return null;
        }
        List<ObjectStyle> stylesChain = getObjectStylesChain();
        for (ObjectStyle style : stylesChain)
        {
            String candidateColor = style.getStrokeColor();
            if (candidateColor != null)
            {
                return candidateColor;
            }
        }
        return null;
    }

    public Double getStrokeTint() throws SelectorDocumentException,
            URISyntaxException, IOException, IdmlLibException
    {
        if (strokeTint == null)
        {
            strokeTint = calculateStrokeTint();
        }
        return strokeTint;
    }

    private Double calculateStrokeTint() throws URISyntaxException,
            IdmlLibException, IOException, SelectorDocumentException
    {
        if (!isEnableStroke())
        {
            return null;
        }
        List<ObjectStyle> stylesChain = getObjectStylesChain();
        for (ObjectStyle style : stylesChain)
        {
            Double candidateTint = style.getStrokeTint();
            if (candidateTint != null)
            {
                return candidateTint;
            }
        }
        return null;
    }

    /**
     * Get the value of strokeTintAndColor
     *
     * @return the value of strokeTintAndColor
     * @throws java.io.IOException
     * @throws java.net.URISyntaxException
     * @throws
     * it.ekr.pdffromselector.parsingengine.exceptions.selectordocumentexceptions.SelectorDocumentException
     * @throws de.fhcon.idmllib.api.IdmlLibException
     */
    public TintAndColorWrapper getStrokeTintAndColor() throws IOException,
            URISyntaxException, SelectorDocumentException, IdmlLibException
    {
        if (strokeTintAndColor == null)
        {
            strokeTintAndColor = calculateStrokeTintAndColor();
        }
        return strokeTintAndColor;
    }

    public TintAndColorWrapper calculateStrokeTintAndColor() throws
            URISyntaxException,
            SelectorDocumentException, IdmlLibException, IOException
    {
        if (!isEnableStroke())
        {
            return null;
        }
        String fillColorOrTintSelf = getStrokeColor();
        TintAndColorWrapper createTintAndColorWrapperBySelf =
                IdmlUtils.createTintAndColorWrapperBySelf(
                        getParentSelectorDocument().getSupertemplateDocument(),
                        fillColorOrTintSelf);
        Double fillTintValue = getStrokeTint();
        if (fillTintValue != null)
        {
            if (createTintAndColorWrapperBySelf != null)
            {
                 createTintAndColorWrapperBySelf.setTintValue(fillTintValue);
            }
           
        }

        return createTintAndColorWrapperBySelf;
    }

    @Override
    public void cleanupCachedData()
    {
        super.cleanupCachedData(); //To change body of generated methods, choose Tools | Templates.
        containedImage = null;
        enableStroke = null;
        objectStyle = null;
        objectStylesChain = null;
        strokeColor = null;
        strokeTint = null;
        strokeTintAndColor = null;
        strokeWeight = null;
    }
    
    
}
