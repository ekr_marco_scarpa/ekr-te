package it.ekr.pdffromselector.parsingengine.selectorobjects;

import it.ekr.pdffromselector.parsingengine.exceptions.selectordocumentexceptions.SelectorDocumentException;
import it.ekr.pdffromselector.parsingengine.exceptions.selectordocumentexceptions.SelectorFileNotExistentException;
import it.ekr.pdffromselector.parsingengine.utils.ArrayUtils;
import it.ekr.pdffromselector.parsingengine.utils.MultikeysDictionary;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.List;
import java.util.logging.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

/**
 *
 * @author Marco Scarpa
 */
public abstract class AbstractSelectorObject implements ISelectorObject
{

    private final ISelectorObject parent;

    protected Node xmlDescription;

    protected List<ISelectorObject> containedObjects = null;
    private List<SelectorFloatingBox> containedSelectorFloatingBoxes = null;
    private Dictionary<String, List<SelectorFloatingBox>> containedFloatingBoxesByType =
            null;
    private List<SelectorTable> containedSelectorTables = null;

    public AbstractSelectorObject(ISelectorObject parent, Node xmlDescription)
    {
        this.parent = parent;
        this.xmlDescription = xmlDescription;
    }

    @Override
    public List<ISelectorObject> getContainedObjectsByTypePath(
            List<String> typePath) throws SelectorDocumentException,
            SelectorFileNotExistentException, ParserConfigurationException,
            SAXException, IOException
    {
        if (objectsByTypePathCache != null)
        {
            List<ISelectorObject> cachedObject =
                    objectsByTypePathCache.get(typePath);//non verifico prima se è già presewnte in modo da risparmiare una ricerca ricorsiva
            if (cachedObject != null)
            {
                return cachedObject;
            }

        }
        return calculateContainedObjectsByTypePath(typePath);

    }

    protected MultikeysDictionary<String, List<ISelectorObject>> objectsByTypePathCache =
            null;

    protected List<ISelectorObject> calculateContainedObjectsByTypePath(
            List<String> typePath) throws SelectorDocumentException,
            SelectorFileNotExistentException, ParserConfigurationException,
            SAXException, IOException
    {
        List<ISelectorObject> toReturn = new ArrayList<>();
        List<ISelectorObject> analyzingContainedObjects = getContainedObjects();
        for (ISelectorObject analyzingObject : analyzingContainedObjects)
        {
            List<String> analyzingTypePath = analyzingObject.getTypePath(null);
            if (ArrayUtils.areStringListsWithSameContent(typePath,
                    analyzingTypePath))
            {
                toReturn.add(analyzingObject);
            }
        }
        if (objectsByTypePathCache == null)
        {
            objectsByTypePathCache = new MultikeysDictionary<>();
        }
        objectsByTypePathCache.put(typePath, toReturn);
        return toReturn;
    }

    @Override
    public ISelectorObject getParent()
    {
        return parent;
    }

    @Override
    public SelectorDocument getParentSelectorDocument()
    {
        ISelectorObject parent1 = getParent();
        if (parent1 != null)
        {
            return parent1.getParentSelectorDocument();
        }
        return null;
    }

    @Override
    public List<ISelectorObject> getContainedObjects() throws
            SelectorDocumentException, SelectorFileNotExistentException,
            ParserConfigurationException, SAXException, IOException
    {
        if (containedObjects == null)
        {
            containedObjects = calculateContainedObjects();
        }
        if (containedObjects == null)
        {
            //se anche dopo il calcolo resta null restituiscxo un null, se no da problemi con collections
            return null;
        }
        return Collections.unmodifiableList(containedObjects);
    }

    /**
     * metodo usato per inizializzare gli oggetti contenuti in modo da non dover
     * elaborare l'xml sempre
     *
     * @return gli oggetti contenuti nell'oggetto corrente
     * @throws SelectorDocumentException
     * @throws SelectorFileNotExistentException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     */
    protected abstract List<ISelectorObject> calculateContainedObjects() throws
            SelectorDocumentException, SelectorFileNotExistentException,
            ParserConfigurationException, SAXException, IOException;

    @Override
    public List<String> getTypePath(ISelectorObject invoker)
    {
        if (invoker == null)
        {
            invoker = this;
        }
        List<String> parentTypePath = null;
        if (parent != null)
        {
            parentTypePath = parent.getTypePath(invoker);
        }
        List<String> currentTypeWrapper = new ArrayList<>();
        String currentType = getType();
        if (currentType != null)
        {
            currentTypeWrapper.add(currentType);
        }
        if (parentTypePath != null)
        {
            //stream di Java 8
            /*List<String> concatenated = Stream.concat(parentTypePath.stream(),
                    currentTypeWrapper.stream()).collect(Collectors.toList());*/
            //in kors non è supportato java 8, va rifatto in java 7
             List<String> concatenated = new ArrayList<>();
            for (String inParentTypePath:parentTypePath)
            {
                concatenated.add(inParentTypePath);
            }
            for (String inCurrentWrapper:currentTypeWrapper)
            {
                concatenated.add(inCurrentWrapper);
            }
            return concatenated;
        }
        return currentTypeWrapper;
    }

    @Override
    public String getType()
    {
        if (xmlDescription != null)
        {
            NamedNodeMap attributes = xmlDescription.getAttributes();
            if (attributes != null)
            {
                Node namedItem = attributes.getNamedItem("tipo");
                if (namedItem != null)
                {
                    String textContent = namedItem.getTextContent();
                    if (!textContent.isEmpty() &&
                            !textContent.equalsIgnoreCase(
                                    "none") && !textContent.equalsIgnoreCase(
                                    "[none]"))
                    {
                        return textContent;
                    }
                }
            }

        }
        return null;
    }

    protected List<SelectorParagraph> containedSelectorParagraphs = null;
    protected List<SelectorParagraph> containedSelectorParagraphsExcludedTableOnes = null;

    @Override
    public List<SelectorParagraph> getContainedSelectorParagraphs() throws
            SelectorDocumentException, SelectorFileNotExistentException,
            ParserConfigurationException, SAXException, IOException
    {
        if (containedSelectorParagraphs == null)
        {
            containedSelectorParagraphs = calculateContainedSelectorParagraphs();
        }
        return containedSelectorParagraphs;
    }

    protected List<SelectorParagraph> calculateContainedSelectorParagraphs()
            throws SAXException, IOException, ParserConfigurationException,
            SelectorDocumentException
    {
        List<SelectorParagraph> toReturn = new ArrayList<>();
        List<ISelectorObject> currentContainedObjects = getContainedObjects();
        for (ISelectorObject contained : currentContainedObjects)
        {
            List<SelectorParagraph> currentFound = contained.
                    getContainedSelectorParagraphs();
            toReturn.addAll(currentFound);
        }
        return toReturn;
    }

    @Override
    public List<SelectorParagraph> getContainedSelectorParagraphsExcludingTableOnes()
            throws SelectorDocumentException, SelectorFileNotExistentException,
            ParserConfigurationException, SAXException, IOException
    {
       if (containedSelectorParagraphsExcludedTableOnes == null)
       {
           containedSelectorParagraphsExcludedTableOnes = calculateContainedSelectorParagraphsExcludingTableOnes();
       }
       return containedSelectorParagraphsExcludedTableOnes;
    }

    @Override
    public List<SelectorParagraph> getContainedSelectorParagraphs(
                                                                  boolean excludeTableParagraphs)
            throws SelectorDocumentException, SelectorFileNotExistentException,
            ParserConfigurationException, SAXException, IOException
    {
        if (excludeTableParagraphs)
        {
            return getContainedSelectorParagraphsExcludingTableOnes();
        }
        return getContainedSelectorParagraphs();
    }
    
     protected List<SelectorParagraph> calculateContainedSelectorParagraphsExcludingTableOnes()
            throws SAXException, IOException, ParserConfigurationException,
            SelectorDocumentException
    {
        List<SelectorParagraph> toReturn = new ArrayList<>();
        if (containedSelectorParagraphs != null)
        {
            //visto che ho già navigato per cercare i paragrafi, cerco quelli che non hanno tabelle come antenati
            for (SelectorParagraph storedPar:containedSelectorParagraphs)
            {
                SelectorTable storedParTableAncestor = storedPar.getTableAncestor();
                if (storedParTableAncestor == null)
                {
                    toReturn.add(storedPar);
                }
            }
            return toReturn;
        }
        //se non avevo già navigato per cercare i paragrafi, cerco quelli che non sono in tabella
        List<ISelectorObject> currentContainedObjects = getContainedObjects();
        for (ISelectorObject contained : currentContainedObjects)
        {
            List<SelectorParagraph> currentFound = contained.
                    getContainedSelectorParagraphsExcludingTableOnes();
            if (currentFound != null)
            {
                toReturn.addAll(currentFound);
            }
            
        }
        return toReturn;
    }

     protected SelectorTable tableAncestor = null;
     
    @Override
    public SelectorTable getTableAncestor() throws SelectorDocumentException,
            SelectorFileNotExistentException, ParserConfigurationException,
            SAXException, IOException
    {
        if(tableAncestor == null)
        {
            tableAncestor = calculateTableAncestor();
        }
        return tableAncestor;
    }
    
     private SelectorTable calculateTableAncestor() throws SelectorDocumentException, SelectorFileNotExistentException, ParserConfigurationException, SAXException, IOException
    {
       ISelectorObject currentParent = getParent();
       if (currentParent != null)
       {
         return  currentParent.getTableAncestor();
       }
       return null;
    }

    @Override
    public List<SelectorTable> getContainedSelectorTables() throws
            SelectorDocumentException,
            SelectorFileNotExistentException,
            ParserConfigurationException, SAXException, IOException
    {
        if (containedSelectorTables == null)
        {
            containedSelectorTables = calculateContainedSelectorTables();
        }
        return containedSelectorTables;
    }

    protected List<SelectorTable> calculateContainedSelectorTables() throws
            IOException, ParserConfigurationException, SelectorDocumentException,
            SAXException
    {
        List<SelectorTable> toReturn = new ArrayList<>();
        List<ISelectorObject> currentContainedObjects = getContainedObjects();
        if (currentContainedObjects != null)
        {
            for (ISelectorObject contained : currentContainedObjects)
            {
                List<SelectorTable> currentFound = contained.
                        getContainedSelectorTables();
                toReturn.addAll(currentFound);
            }
        }

        return toReturn;
    }

    @Override
    public SelectorPage getParentPage()
    {
        ISelectorObject currentParent = getParent();
        if (currentParent != null)
        {
            return currentParent.getParentPage();
        }
        return null;
    }

    @Override
    public List<SelectorFloatingBox> getContainedSelectorFloatingBoxes() throws
            SelectorDocumentException, SelectorFileNotExistentException,
            ParserConfigurationException, SAXException, IOException
    {
        if (containedSelectorFloatingBoxes == null)
        {
            containedSelectorFloatingBoxes =
                    calculateContainedSelectorFloatingBoxes();
        }
        return Collections.unmodifiableList(containedSelectorFloatingBoxes);
    }

    private List<SelectorFloatingBox> calculateContainedSelectorFloatingBoxes()
            throws SelectorDocumentException, SelectorFileNotExistentException,
            ParserConfigurationException, SAXException, IOException
    {
        List<SelectorFloatingBox> toReturn = new ArrayList<>();
        List<ISelectorObject> currentContainedObjects = getContainedObjects();
        if (currentContainedObjects != null)
        {
            for (ISelectorObject contained : currentContainedObjects)
            {
                List<SelectorFloatingBox> fromChildSelectorFloatingBoxes =
                        contained.getContainedSelectorFloatingBoxes();
                if ((fromChildSelectorFloatingBoxes != null) &&
                        (fromChildSelectorFloatingBoxes.size() > 0))
                {
                    toReturn.addAll(fromChildSelectorFloatingBoxes);
                }

            }
        }

        return toReturn;
    }

    @Override
    public List<SelectorFloatingBox> getContainedSelectorFloatingBoxesByType(
            String searchingType) throws SelectorDocumentException,
            SelectorFileNotExistentException, ParserConfigurationException,
            SAXException, IOException
    {
        return calculateContainedSelectorFloatingBoxesByType(searchingType);
    }

    private List<SelectorFloatingBox> calculateContainedSelectorFloatingBoxesByType(
            String searchingType) throws SelectorDocumentException,
            SelectorFileNotExistentException, ParserConfigurationException,
            SAXException, IOException
    {
        if (containedFloatingBoxesByType == null)
        {
            containedFloatingBoxesByType = new Hashtable<>();
        }
        List<SelectorFloatingBox> storedByType =
                containedFloatingBoxesByType.get(searchingType);
        if (storedByType == null)
        {
            storedByType = new ArrayList<>();
            List<SelectorFloatingBox> containedBoxes =
                    getContainedSelectorFloatingBoxes();
            for (SelectorFloatingBox box : containedBoxes)
            {
                String boxType = box.getType();
                if (boxType != null)
                {
                    if (boxType.equals(searchingType))
                    {
                        storedByType.add(box);
                    }
                } else
                {
                    //verifico se entrambi sono null
                    if (boxType == searchingType)
                    {
                        storedByType.add(box);
                    }
                }

            }
            containedFloatingBoxesByType.put(searchingType, storedByType);

        }
        return storedByType;

    }

    @Override
    public void cleanupCachedData()
    {
        try
        {
            List<ISelectorObject> containedObjects1 = getContainedObjects();
            if ((containedObjects1 != null) &&(!containedObjects1.isEmpty()))
            {
                for (ISelectorObject cont : containedObjects1)
                {
                    cont.cleanupCachedData();
                }
            }

        } catch (SelectorDocumentException ex)
        {
            Logger.getLogger(AbstractSelectorObject.class.getName()).
                    log(Level.SEVERE, null, ex);
        } catch (ParserConfigurationException ex)
        {
            Logger.getLogger(AbstractSelectorObject.class.getName()).
                    log(Level.SEVERE, null, ex);
        } catch (SAXException ex)
        {
            Logger.getLogger(AbstractSelectorObject.class.getName()).
                    log(Level.SEVERE, null, ex);
        } catch (IOException ex)
        {
            Logger.getLogger(AbstractSelectorObject.class.getName()).
                    log(Level.SEVERE, null, ex);
        }
        containedFloatingBoxesByType = null;
        containedObjects = null;
        containedSelectorFloatingBoxes = null;
        containedSelectorParagraphs = null;
        containedSelectorTables = null;
        objectsByTypePathCache = null;
        tableAncestor = null;
        containedSelectorParagraphsExcludedTableOnes = null;

    }

   

}
