package it.ekr.pdffromselector.parsingengine.selectorobjects;

import de.fhcon.idmllib.api.IdmlLibException;
import it.ekr.pdffromselector.parsingengine.exceptions.selectordocumentexceptions.SelectorDocumentException;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

/**
 *
 * @author Marco Scarpa
 */
public class SelectorInlineImage extends AbstractSelectorImage
{

    private double height = 0;

    private double width = 0;

    /**
     *
     * @param parent
     * @param xmlDescription
     * @param precedingTypes
     */
    public SelectorInlineImage(ISelectorObject parent, Node xmlDescription,
                               List<String> precedingTypes)
    {
        super(parent, xmlDescription, precedingTypes);

    }

    @Override
    public URI getHref() throws URISyntaxException
    {

        Node fileTag = xmlDescription.getFirstChild();
        NamedNodeMap attributes = fileTag.getAttributes();
        Node namedItem = attributes.getNamedItem("href");
        String textContent = namedItem.getTextContent();
        if (textContent.startsWith("file:/"))
        {
            return new URI(textContent);
        } else
        {
            File wrapper = new File(textContent);
            return wrapper.toURI();
        }

    }

    @Override
    public double getAvailableHeight() throws URISyntaxException, IOException,
            IdmlLibException, SelectorDocumentException
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double getAvailableWidth() throws URISyntaxException,
            IOException, IdmlLibException,
            SelectorDocumentException
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double getHeight() throws URISyntaxException, IOException,
            IdmlLibException,
            SelectorDocumentException
    {
        if (height == 0)
        {
            height = calculateHeight();
        }
        return height;
    }

    @Override
    public double getWidth() throws URISyntaxException, IOException,
            IdmlLibException,
            SelectorDocumentException
    {
        if (width == 0)
        {
            width = calculateWidth();
        }
        return width;
    }

    /**
     *
     * @return
     */
    protected double calculateHeight()
    {
        NamedNodeMap attributes = xmlDescription.getAttributes();
        Node heightAttribute = attributes.getNamedItem("altezza");
        String textContent = heightAttribute.getTextContent();
        Double toReturnWrapped = new Double(textContent);
        return toReturnWrapped;
    }

    /**
     *
     * @return
     */
    protected double calculateWidth()
    {
        NamedNodeMap attributes = xmlDescription.getAttributes();
        Node heightAttribute = attributes.getNamedItem("larghezza");
        String textContent = heightAttribute.getTextContent();
        Double toReturnWrapped = new Double(textContent);
        return toReturnWrapped;
    }

    @Override
    public void cleanupCachedData()
    {
        super.cleanupCachedData(); //To change body of generated methods, choose Tools | Templates.
        height = 0;
        width = 0;
    }


    
}
