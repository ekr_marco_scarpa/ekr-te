/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.ekr.pdffromselector.parsingengine.selectorobjects;

import it.ekr.pdffromselector.parsingengine.exceptions.selectordocumentexceptions.SelectorDocumentException;
import it.ekr.pdffromselector.parsingengine.exceptions.selectordocumentexceptions.SelectorFileNotExistentException;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author Marco Scarpa
 */
public class SelectorFascicle extends AbstractSelectorObject implements
        ISelectorObject
{

    private Path pagesFolder = null;
    private String fascicleMastro = null;
    private SelectorDocument parentSelectorDocument;
    private List<SelectorPage> pages = null;

    private String name = null;

    /**
     *
     * @param fascicleNode
     * @param inhirithedMastro
     * @param pagesFoder
     * @param parentSelectorDocument
     */
    public SelectorFascicle(Node fascicleNode, String inhirithedMastro,
                            Path pagesFoder,
                            SelectorDocument parentSelectorDocument)
    {
        super(parentSelectorDocument, fascicleNode);
        this.pagesFolder = pagesFoder;
        this.parentSelectorDocument = parentSelectorDocument;

        NamedNodeMap attributes = fascicleNode.getAttributes();
        Node riferimentoMastroAttribute = attributes.getNamedItem(
                "riferimento_mastro");
        fascicleMastro = inhirithedMastro;
        if (riferimentoMastroAttribute != null)
        {
            fascicleMastro = riferimentoMastroAttribute.getNodeValue();
        }

    }

    @Override
    public SelectorDocument getParentSelectorDocument()
    {
        return parentSelectorDocument;
    }

    @Override
    public ISelectorObject getParent()
    {
        return getParentSelectorDocument();
    }

    /**
     * Get the value of name
     *
     * @return the value of name
     */
    public String getName()
    {
        if (name == null)
        {
            name = calculateName();
        }
        return name;
    }

    /**
     *
     * @return
     */
    public String calculateName()
    {
        NamedNodeMap attributes = xmlDescription.getAttributes();
        Node nameAttribute = attributes.getNamedItem("nome");
        if (nameAttribute != null)
        {
            String nameAttributeValue = nameAttribute.getTextContent();
            if (nameAttributeValue != null)
            {
                return nameAttributeValue;
            }
        }
        return null;
    }

    private Node retrievePagineNode(NodeList fascicleChildNodes)
    {
        for (int i = 0; i < fascicleChildNodes.getLength(); i++)
        {
            Node childNode = fascicleChildNodes.item(i);
            if ("pagine".equals(childNode.getNodeName()))
            {
                return childNode;
            }
        }
        return null;
    }

    private List<Node> retrieveRiferimentoPaginaNodes(Node pagineNode)
    {
        List<Node> toReturn = new ArrayList<>();
        NodeList pagineChildNodes = pagineNode.getChildNodes();
        for (int i = 0; i < pagineChildNodes.getLength(); i++)
        {
            Node childNode = pagineChildNodes.item(i);
            if ("riferimento_pagina".equals(childNode.getNodeName()))
            {
                toReturn.add(childNode);
            }
        }
        return toReturn;
    }

    private List<SelectorPage> createPages() throws
            SelectorFileNotExistentException, ParserConfigurationException,
            SAXException, IOException
    {
        List<SelectorPage> creatingPages;
        NodeList childNodes = xmlDescription.getChildNodes();
        Node pagineNode = retrievePagineNode(childNodes);
        List<Node> riferimentoPaginaNodes =
                retrieveRiferimentoPaginaNodes(pagineNode);
        creatingPages = new ArrayList<>();
        for (Node riferimentoPaginaNode : riferimentoPaginaNodes)
        {
            NamedNodeMap attributesRiferimentoPagina = riferimentoPaginaNode.
                    getAttributes();
            Node pageNameAttribute = attributesRiferimentoPagina.getNamedItem(
                    "nome");
            if (pageNameAttribute != null)
            {
                String pageFileName = pageNameAttribute.getTextContent();
                pageFileName += ".xml";
                Path pagePath = pagesFolder.resolve(pageFileName);
                File pageFile = pagePath.toFile();
                SelectorPage creatingPage = new SelectorPage(pageFile,
                        fascicleMastro, riferimentoPaginaNode, this);
                creatingPages.add(creatingPage);
            }
        }
        return creatingPages;
    }

    /**
     * pagine contenute nel fascicolo
     *
     * @return the value of pages
     * @throws
     * it.ekr.pdffromselector.parsingengine.exceptions.selectordocumentexceptions.SelectorFileNotExistentException
     * @throws javax.xml.parsers.ParserConfigurationException
     */
    public List<SelectorPage> getPages() throws
            SelectorFileNotExistentException,
            ParserConfigurationException, SAXException, IOException
    {
        if (pages == null)
        {
            pages = createPages();
        }
        if (pages == null)
        {
            //se anche dopo il calcolo resta null
            return null;
        }
        return Collections.unmodifiableList(pages);
    }

    @Override
    public List<ISelectorObject> getContainedObjects() throws
            SelectorDocumentException,
            SelectorFileNotExistentException,
            ParserConfigurationException, SAXException, IOException
    {
        List<ISelectorObject> toReturn;
        List<SelectorPage> currentPages = getPages();
        toReturn = new ArrayList<>();
        for (SelectorPage page:currentPages)
        {
            toReturn.add(page);
        }
        return toReturn;
    }

    @Override
    protected List<ISelectorObject> calculateContainedObjects() throws
            SelectorDocumentException,
            SelectorFileNotExistentException,
            ParserConfigurationException, SAXException, IOException
    {
        List<SelectorPage> currentPages = getPages();
        List<ISelectorObject> toReturn = new ArrayList<>();
        for (SelectorPage page:currentPages)
        {
            toReturn.add(page);
        }
        return toReturn;
    }

    /**
     *
     * @return
     */
    @Override
    public SelectorPage getParentPage()
    {
        return null;
    }

    @Override
    public void cleanupCachedData()
    {
        super.cleanupCachedData();
        pages = null;
    }
    
    

}
