/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.ekr.pdffromselector.parsingengine.selectorobjects;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

/**
 *
 * @author Marco Scarpa
 */
public abstract class AbstractSelectorUnit extends AbstractSelectorObject
        implements ISelectorObject
{

    /**
     *
     * @param parent
     * @param unitXml
     */
    public AbstractSelectorUnit(ISelectorObject parent, Node unitXml)
    {
        super(parent, unitXml);
    }

    @Override
    public List<String> getTypePath(ISelectorObject invoker)
    {
        if (invoker == null)
        {
            invoker = this;
        }
        //sia le unit flusso che le unit space hanno lo stesso attributo per il tipo risolto
        NamedNodeMap attributes = xmlDescription.getAttributes();
        Node namedItem = attributes.getNamedItem("tipo_risolto");
        //visto che le unit sono anche i contenuti delle celle di tabella, non è detto che abbiano l'attributo tipo_risolto
        if (namedItem != null)
        {
            String textContent = namedItem.getTextContent();
            String[] split = textContent.split("\\.");
            ArrayList<String> toReturn = new ArrayList<>();
            toReturn.addAll(Arrays.asList(split));
            return toReturn;
        }
        //per le unit usate come contenuti di cella devo procedere in maniera come precedente
        return super.getTypePath(invoker);
    }

}
