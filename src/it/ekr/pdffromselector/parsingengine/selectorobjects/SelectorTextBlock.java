/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.ekr.pdffromselector.parsingengine.selectorobjects;

import it.ekr.pdffromselector.parsingengine.exceptions.selectordocumentexceptions.SelectorDocumentException;
import it.ekr.pdffromselector.parsingengine.exceptions.selectordocumentexceptions.SelectorFileNotExistentException;
import it.ekr.pdffromselector.parsingengine.utils.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

/**
 *
 * @author Marco Scarpa
 */
public class SelectorTextBlock extends AbstractSelectorObject
{

    /**
     *
     * @param parent
     * @param xmlDescription
     */
    public SelectorTextBlock(ISelectorObject parent, Node xmlDescription)
    {
        super(parent, xmlDescription);
    }

    @Override
    public List<ISelectorObject> calculateContainedObjects() throws
            SelectorDocumentException, SelectorFileNotExistentException,
            ParserConfigurationException, SAXException, IOException
    {
        List<ISelectorObject> toReturn = new ArrayList<>();
        List<Node> childNodes = XmlManagement.retrieveElementChildren(
                xmlDescription);
        for (int i = 0; i < childNodes.size(); i++)
        {
            Node childNode = childNodes.get(i);
            SelectorParagraph creatingParagraph = new SelectorParagraph(this,
                    childNode);
            toReturn.add(creatingParagraph);
        }
        return toReturn;
    }

    @Override
    public void cleanupCachedData()
    {
        super.cleanupCachedData(); //To change body of generated methods, choose Tools | Templates.
    }

    

    
}
