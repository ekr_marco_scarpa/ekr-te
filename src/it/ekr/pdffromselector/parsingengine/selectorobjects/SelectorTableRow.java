/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.ekr.pdffromselector.parsingengine.selectorobjects;

import de.fhcon.idmllib.api.IdmlLibException;
import it.ekr.pdffromselector.parsingengine.exceptions.selectordocumentexceptions.SelectorDocumentException;
import it.ekr.pdffromselector.parsingengine.exceptions.selectordocumentexceptions.SelectorFileNotExistentException;
import it.ekr.pdffromselector.parsingengine.utils.XmlManagement;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author Marco Scarpa
 */
public class SelectorTableRow extends AbstractSelectorContainer
{

    private String extraRowStyle = null;// dal codice di selector sembra che in caso di extra stile riga sovrascriva il tipo della riga

    private Boolean header = null;
    private List<SelectorTableCell> cells = null;
    private Double percentagesWidthSum = null;
    private Double fixedWidthSum = null;
    private List<Double> cellsWidths = null;

    /**
     *
     * @param parent
     * @param xmlDescription
     */
    public SelectorTableRow(ISelectorObject parent, Node xmlDescription)
    {
        super(parent, xmlDescription);
    }

    /**
     * il valore di extra stile riga
     *
     * @return lo stile riga extra
     */
    public String getExtraRowStyle()
    {
        if (extraRowStyle == null)
        {
            extraRowStyle = calculateExtraRowStyle();
        }
        return extraRowStyle;
    }

    private String calculateExtraRowStyle()
    {
        NamedNodeMap attributes = xmlDescription.getAttributes();
        Node namedItem = attributes.getNamedItem("extra_stile_riga");
        if (namedItem != null)
        {
            String textContent = namedItem.getTextContent();
            if (!textContent.isEmpty() && !textContent.equalsIgnoreCase(
                    "none") && !textContent.equalsIgnoreCase("[none]"))
            {
                return textContent;
            }
        }
        return null;
    }

    @Override
    public String getType()
    {
        if (getExtraRowStyle() != null)
        {
            return getExtraRowStyle();
        }
        return super.getType();
    }

    /**
     * dice se la riga è una riga di intestazione
     *
     * @return true se la riga è di intestazione
     */
    public boolean isHeader()
    {

        if (header == null)
        {
            header = calculateHeaderFlag();
        }
        return header;
    }

    private boolean calculateHeaderFlag()
    {
        NamedNodeMap attributes = xmlDescription.getAttributes();
        Node namedItem = attributes.getNamedItem("intestazione");
        if (namedItem != null)
        {
            String textContent = namedItem.getTextContent();
            return XmlManagement.convertStringToBoolean(textContent);
        }
        return false;
    }

    @Override
    public List<ISelectorObject> calculateContainedObjects() throws
            SelectorDocumentException, SelectorFileNotExistentException,
            ParserConfigurationException, SAXException, IOException
    {
        List<ISelectorObject> toReturn = new ArrayList<>();
        List<Node> childNodes = XmlManagement.retrieveElementChildren(
                xmlDescription);
        for (int i = 0; i < childNodes.size(); i++)
        {
            Node cellTag = childNodes.get(i);
            SelectorTableCell creatingCell = new SelectorTableCell(this,
                    cellTag);
            toReturn.add(creatingCell);
        }
        return toReturn;
    }

    @Override
    public double getAvailableHeight() throws URISyntaxException,
            IOException, IdmlLibException,
            SelectorDocumentException
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double getAvailableWidth() throws URISyntaxException,
            IOException, IdmlLibException,
            SelectorDocumentException
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double getHeight() throws URISyntaxException, IOException,
            IdmlLibException,
            SelectorDocumentException
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double getWidth() throws URISyntaxException, IOException,
            IdmlLibException,
            SelectorDocumentException
    {
        //la larghezza della riga è pari alla larghezza della tabella padre
        SelectorTable parentTable = getParentTable();
        double width = parentTable.getWidth();
        return width;
    }

    
    public SelectorTable getParentTable()
    {
        return (SelectorTable) getParent();
    }
    /**
     * Get the value of cells
     *
     * @return the value of cells
     * @throws
     * it.ekr.pdffromselector.parsingengine.exceptions.selectordocumentexceptions.SelectorDocumentException
     * @throws
     * it.ekr.pdffromselector.parsingengine.exceptions.selectordocumentexceptions.SelectorFileNotExistentException
     * @throws javax.xml.parsers.ParserConfigurationException
     * @throws org.xml.sax.SAXException
     * @throws java.io.IOException
     */
    public List<SelectorTableCell> getCells() throws SelectorDocumentException,
            SelectorFileNotExistentException, ParserConfigurationException,
            SAXException, IOException
    {
        if (cells == null)
        {
            cells = calculateCells();
        }
        return Collections.unmodifiableList(cells);
    }

    private List<SelectorTableCell> calculateCells() throws
            SelectorDocumentException, SelectorFileNotExistentException,
            ParserConfigurationException, SAXException, IOException
    {
        List<ISelectorObject> objectivezCells = getContainedObjects();
        List<SelectorTableCell> toReturn = new ArrayList<>();
        /*
        objectivezCells.stream().
                forEach((objCell) ->
                        {
                            toReturn.add((SelectorTableCell) objCell);
                });*/
        //KORS non supporta java 8, va rifatto in java 7
        for (ISelectorObject obj:objectivezCells)
        {
            SelectorTableCell cell = (SelectorTableCell) obj;
            toReturn.add(cell);
        }
        return toReturn;
    }

    
    public Double getPercentagesWidthSum() throws SelectorDocumentException, SelectorFileNotExistentException, ParserConfigurationException, SAXException, IOException
    {
        if (percentagesWidthSum == null)
        {
            percentagesWidthSum = calculatePercentagesWidthSum();
        }
        return percentagesWidthSum;
    }

    protected Double calculatePercentagesWidthSum() throws SAXException,
            SelectorDocumentException, ParserConfigurationException, IOException
    {
        boolean calculatingPercSum = true;
        return calculateCellWidthSum(calculatingPercSum);
    }

    protected Double calculateCellWidthSum(boolean calculatingPercSum) throws
            IOException, SelectorDocumentException, SAXException,
            ParserConfigurationException
    {
        List<SelectorTableCell> currentCells = getCells();
        Double acc = (double) 0;
        for (SelectorTableCell cell:currentCells)
        {
            boolean considerCell = tellIfCellMustConsideredInSum(cell, calculatingPercSum);
            if(considerCell)
            {
                Double storedWidth = cell.getStoredWidth();
                acc += storedWidth;
            }
        }
        return acc;
    }

    protected boolean tellIfCellMustConsideredInSum(SelectorTableCell cell,
                                                    boolean calculatingPercSum)
    {
        boolean considerCell = cell.isStoredWidthPercentage();
        if (!calculatingPercSum)
        {
            considerCell = ! cell.isStoredWidthPercentage();
        }
        return considerCell;
    }
    
    public Double getFixedWidthSum() throws IOException, SelectorDocumentException, SAXException, ParserConfigurationException
    {
        if (fixedWidthSum == null)
        {
             fixedWidthSum = calculateFixedWidthSum();
        }
        return fixedWidthSum;
    }

    protected Double calculateFixedWidthSum() throws SelectorDocumentException,
            IOException, ParserConfigurationException, SAXException
    {
        boolean calculatingPercSum = false;
        return calculateCellWidthSum(calculatingPercSum);
    }
    
    public List<Double> getCellsWidths() throws SelectorDocumentException, SelectorFileNotExistentException, ParserConfigurationException, SAXException, IOException, URISyntaxException, IdmlLibException
    {
        if (cellsWidths == null)
        {
             cellsWidths = calculateCellsWidths();
        }
        return cellsWidths;
    }

    protected List<Double> calculateCellsWidths() throws IOException,
            SAXException, URISyntaxException, IdmlLibException,
            SelectorDocumentException, ParserConfigurationException
    {
        List<SelectorTableCell> currentCells= getCells();
        List<Double> toReturn = new ArrayList<>();
        for (SelectorTableCell cell:currentCells)
        {
            double cellWidth = cell.getWidth();
            toReturn.add(cellWidth);
        }
        return toReturn;
    }

    @Override
    public void cleanupCachedData()
    {
        super.cleanupCachedData(); //To change body of generated methods, choose Tools | Templates.
        cells = null;
        cellsWidths = null;
        extraRowStyle = null;
        fixedWidthSum = null;
        header = null;
        percentagesWidthSum = null;
        
    }
    
    
    
}
