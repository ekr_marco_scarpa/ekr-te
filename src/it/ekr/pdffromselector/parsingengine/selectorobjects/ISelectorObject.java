/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.ekr.pdffromselector.parsingengine.selectorobjects;

import it.ekr.pdffromselector.parsingengine.exceptions.selectordocumentexceptions.SelectorDocumentException;
import it.ekr.pdffromselector.parsingengine.exceptions.selectordocumentexceptions.SelectorFileNotExistentException;
import java.io.IOException;
import java.util.List;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

/**
 *
 * @author Marco Scarpa
 */
public interface ISelectorObject
{

    /**
     * fornisce il percorso tipi di questo oggetto
     *
     * @param invoker l'oggetto da cui parte la richiesta di type path (ad
     * esempio nello stile di paragrafo bisogna saltare il tipo della cella)
     * @return il percorso tipi
     */
    List<String> getTypePath(ISelectorObject invoker);

    /**
     * fornisce i figli dell'oggetto corrente (no ricerca ricorsiva)
     *
     * @return gli oggetti contenuti dall'oggetto corrente
     * @throws SelectorDocumentException
     * @throws SelectorFileNotExistentException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     */
    List<ISelectorObject> getContainedObjects() throws SelectorDocumentException,
            SelectorFileNotExistentException, ParserConfigurationException,
            SAXException, IOException;

    /**
     * fornisce il documento di selector a cui questo oggetto appartiene
     *
     * @return il documento di Selector che contiene l'oggetto corrente
     */
    SelectorDocument getParentSelectorDocument();

    /**
     * fornisce l'oggetto a cui appartiene l'oggetto corrente
     *
     * @return il padre dell'oggetto corrente
     */
    ISelectorObject getParent();

    /**
     * fornisce il tipo dell'oggetto corrente
     *
     * @return il tipo dell'oggetto corrente
     */
    String getType();

    /**
     *
     * @return @throws SelectorDocumentException
     * @throws SelectorFileNotExistentException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     */
    List<SelectorParagraph> getContainedSelectorParagraphs() throws
            SelectorDocumentException,
            SelectorFileNotExistentException,
            ParserConfigurationException, SAXException, IOException;
    
    List<SelectorParagraph> getContainedSelectorParagraphsExcludingTableOnes()throws
            SelectorDocumentException,
            SelectorFileNotExistentException,
            ParserConfigurationException, SAXException, IOException;
    
    List<SelectorParagraph> getContainedSelectorParagraphs(boolean excludeTableParagraphs) throws
            SelectorDocumentException,
            SelectorFileNotExistentException,
            ParserConfigurationException, SAXException, IOException;

    /**
     *
     * @return @throws SelectorDocumentException
     * @throws SelectorFileNotExistentException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     */
    List<SelectorTable> getContainedSelectorTables() throws
            SelectorDocumentException,
            SelectorFileNotExistentException,
            ParserConfigurationException, SAXException, IOException;

    /**
     *
     * @param typePath
     * @return
     * @throws SelectorDocumentException
     * @throws SelectorFileNotExistentException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     */
    List<ISelectorObject> getContainedObjectsByTypePath(List<String> typePath)
            throws SelectorDocumentException,
            SelectorFileNotExistentException, ParserConfigurationException,
            SAXException, IOException;

    /**
     *
     * @return
     */
    SelectorPage getParentPage();

    /**
     *
     * @return @throws SelectorDocumentException
     * @throws SelectorFileNotExistentException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     */
    List<SelectorFloatingBox> getContainedSelectorFloatingBoxes() throws
            SelectorDocumentException, SelectorFileNotExistentException,
            ParserConfigurationException, SAXException, IOException;

    /**
     *
     * @param searchingType
     * @return
     * @throws SelectorDocumentException
     * @throws SelectorFileNotExistentException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     */
    List<SelectorFloatingBox> getContainedSelectorFloatingBoxesByType(
            String searchingType) throws SelectorDocumentException,
            SelectorFileNotExistentException, ParserConfigurationException,
            SAXException, IOException;
    
    SelectorTable getTableAncestor() throws SelectorDocumentException,
            SelectorFileNotExistentException, ParserConfigurationException,
            SAXException, IOException;
    
    void cleanupCachedData();
    
    
}
