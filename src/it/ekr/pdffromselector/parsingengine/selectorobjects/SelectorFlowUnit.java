/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.ekr.pdffromselector.parsingengine.selectorobjects;

import it.ekr.pdffromselector.parsingengine.exceptions.selectordocumentexceptions.SelectorDocumentException;
import it.ekr.pdffromselector.parsingengine.exceptions.selectordocumentexceptions.SelectorFileNotExistentException;
import it.ekr.pdffromselector.parsingengine.utils.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author Marco Scarpa
 */
public class SelectorFlowUnit extends AbstractSelectorUnit
{

    /**
     *
     */
    public static final String FLOW_UNIT_TAG = "flusso";

    /**
     *
     * @param parent
     * @param unitXml
     */
    public SelectorFlowUnit(ISelectorObject parent, Node unitXml)
    {
        super(parent, unitXml);
    }

    @Override
    protected List<ISelectorObject> calculateContainedObjects() throws
            SelectorDocumentException, SelectorFileNotExistentException,
            ParserConfigurationException, SAXException, IOException
    {
        List<ISelectorObject> toReturn = new ArrayList<>();
        List<Node> childNodes = XmlManagement.retrieveElementChildren(
                xmlDescription);
        for (int i = 0; i < childNodes.size(); i++)
        {
            Node itemComponentOrSetup = childNodes.get(i);
            if (itemComponentOrSetup.getNodeName().equals("componente"))
            {
                List<ISelectorObject> createdComponent = processComponente(
                        itemComponentOrSetup);
                if (createdComponent != null)
                {
                    toReturn.addAll(createdComponent);
                }

            }
        }

        return toReturn;
    }

    private List<ISelectorObject> processComponente(Node componente)
    {
        List<ISelectorObject> toReturn = new ArrayList<>();
        List<Node> childNodes = XmlManagement.retrieveElementChildren(componente);
        for (int i = 0; i < childNodes.size(); i++)
        {
            Node currentComponenteChild = childNodes.get(i);
            ISelectorObject toAddChild = processComponenteChild(
                    currentComponenteChild);
            if (toAddChild != null)
            {
                toReturn.add(toAddChild);
            }
            
        }

        return toReturn;
    }

    private ISelectorObject processComponenteChild(Node componenteChild)
    {
        String nodeName = componenteChild.getNodeName();
        switch (nodeName)
        {
            case "blocco_testo":
                ISelectorObject textBlock = processBloccoTesto(componenteChild);
                return textBlock;

            case "tabella":
                ISelectorObject table = processTabella(componenteChild);
                return table;

        }

        return null;
    }

    private ISelectorObject processBloccoTesto(Node bloccoTesto)
    {
        SelectorTextBlock textBlock = new SelectorTextBlock(this, bloccoTesto);
        return textBlock;

    }

    private ISelectorObject processTabella(Node tabella)
    {
        SelectorTable toReturn = new SelectorTable(this, tabella);
        return toReturn;
    }

    @Override
    public void cleanupCachedData()
    {
        super.cleanupCachedData(); //To change body of generated methods, choose Tools | Templates.
    }

    
   
}
