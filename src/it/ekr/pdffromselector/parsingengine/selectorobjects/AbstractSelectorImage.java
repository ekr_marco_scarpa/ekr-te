/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.ekr.pdffromselector.parsingengine.selectorobjects;

import it.ekr.pdffromselector.parsingengine.exceptions.selectordocumentexceptions.SelectorDocumentException;
import it.ekr.pdffromselector.parsingengine.exceptions.selectordocumentexceptions.SelectorFileNotExistentException;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

/**
 *
 * @author Marco Scarpa
 */
public abstract class AbstractSelectorImage extends AbstractSelectorContainer
        implements ISelectorImage
{

    /**
     *
     */
    protected List<String> precedingTypes = null;

    /**
     *
     * @param parent
     * @param xmlDescription
     * @param precedingTypes
     */
    public AbstractSelectorImage(ISelectorObject parent, Node xmlDescription,
                                 List<String> precedingTypes)
    {
        super(parent, xmlDescription);
        this.precedingTypes = precedingTypes;
    }

    @Override
    protected List<ISelectorObject> calculateContainedObjects() throws
            SelectorDocumentException, SelectorFileNotExistentException,
            ParserConfigurationException, SAXException, IOException
    {
        //dentro l'immagine non c'è nulla quindi restituisco null
        return null;
    }

    @Override
    public URI getHref() throws URISyntaxException
    {
        Node fileTag = xmlDescription.getFirstChild();
        NamedNodeMap attributes = fileTag.getAttributes();
        Node namedItem = attributes.getNamedItem("href");
        String textContent = namedItem.getTextContent();
        return new URI(textContent);
    }

    @Override
    public List<String> getTypePath(ISelectorObject invoker)
    {
        if (invoker == null)
        {
            invoker = this;
        }
        List<String> currentTypeWrapper = new ArrayList<>();
        String currentType = getType();
        if (currentType != null)
        {
            currentTypeWrapper.add(currentType);
        }
        if (precedingTypes != null)
        {
            //stream di Java 8
            /*List<String> concatenated = Stream.concat(precedingTypes.stream(),
                    currentTypeWrapper.stream()).collect(Collectors.toList());*/
            //sul server kors non è supportato java 8, bisogna rifare in java 7
            List<String> concatenated = new ArrayList<>();
            for (String inPreceding:precedingTypes)
            {
                concatenated.add(inPreceding);
            }
            for (String inCurrentWrapper:currentTypeWrapper)
            {
                concatenated.add(inCurrentWrapper);
            }
            return concatenated;
        }
        return currentTypeWrapper;
    }

}
