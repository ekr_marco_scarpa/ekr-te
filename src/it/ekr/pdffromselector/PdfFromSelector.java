package it.ekr.pdffromselector;

import com.ekrpe.scheduler.model.LoggerVO;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.itextpdf.text.*;
import de.fhcon.idmllib.api.*;
import it.ekr.pdffromselector.parsingengine.*;
import it.ekr.pdffromselector.parsingengine.exceptions.sapfileexceptions.*;
import it.ekr.pdffromselector.parsingengine.exceptions.selectordocumentexceptions.*;
import java.io.*;
import java.net.*;
import java.nio.file.*;
import java.util.*;
import java.util.logging.*;
import javax.xml.parsers.*;
import org.apache.commons.cli.*;
import org.w3c.dom.DOMException;
import org.xml.sax.SAXException;

/**
 *
 * @author Marco Scarpa
 */
public class PdfFromSelector
{

    /**
     * opzione da riga di comando per indicare il percorso del dfile XML da SAP
     */
    public static String XML_FROM_SAP_OPTION = "xmlDaSap";
    
    public static String CONFIG_FILE_PATH_OPTION = "file_config";
    
    public static String ROOT_PE_OPTION = "root_pe";

    /**
     * opzione da riga di comando per chiedere di mostrare l'help
     */
    public static String HELP_OPTION = "?";

    /**
     * opzione da riga di comando per indicare in che cartella si trovano i documenti di Selector
     */
    public static final String CARTELLA_TEMPLATE = "cartella_template";

    /**
     * opzione da linea di comando per indicare il codice di licenza di IDMLLIB
     */
    public static final String IDML_LIB_LICENSE_OPTION = "licenza_idmllib";
    
    public static final String PDF_SEND_URL_OPTION = "url_invio_pdf";

    public static final String PDF_POST_EXAMPLE_OPTION ="esempio_post_pdf";
    
    public static final String TO_DELETE_FILES_OPTION = "da_cancellare";
    /**
     * codice di licenza di IDMLLIB di default acquistato
     */
    public static final String IDML_LIB_LICENSE =
            "ILJD11-AEA1EFBG-000547-3FC6F84-D4718D9";
    
    public static String EMPTY_ESCAPE_OPTION = "escape_stringa_vuota";

    
    public static String ROOT_PE_ENV_VAR = "ROOT_PE";
    public static String DEFAULT_ROOT_PE = "C:\\EKR-PE";
    public static String TE_SETTINGS_SUBDIR = "settaggi" + File.separator + "TE";
    public static String TE_CONFIG_FILE = "config.conf";
    public static String TEMPLATE_BASE_DIR_PROPERTY = "TEMPLATE_DIR";
    public static String DEFAULT_TEMPLATE_BASE_DIR = "W:\\smartdocs";
    public static String DEFAULT_EMPTY_ESCAPE_SEQUENCE = "|c|";
    public static String EMPTY_ESCAPE_SEQUENCE_PROPERTY = "EMPTY_ESCAPE";
    public static String IDML_LIB_LICENSE_PROPERTY = "IDML_LIB_LICENSE";
    public static String PDF_SEND_URL_PROPERTY = "PDF_SEND_URL";
    public static String PDF_POST_EXAMPLE_PROPERTY = "PDF_POST_EXAMPLE";
    
    /**
     * @param args the command line arguments
     */
    public static void main(
            String[] args)
    {
        // iniziamo dalle opzioni da linea di comando
        Options commandLineOptions = new Options();
        Option helpCommandLineOption = OptionBuilder.withDescription(
                "mostra questo help").create(HELP_OPTION);
        Option sapXmlFileCommandLineOption = OptionBuilder.withArgName(
                "xml_fornito_da_SAP").hasArg().
                withDescription("file xml fornito da SAP").create(
                        XML_FROM_SAP_OPTION);
        Option templateBaseDirOption =
                OptionBuilder.withArgName("dir_template").hasArg().
                        withDescription("cartella contenente i documenti Selector").
                        create(CARTELLA_TEMPLATE);
        Option idmlLibLicenseOption = OptionBuilder.withArgName(
                "licenza_idmllib").hasArg().
                withDescription("licenza per la lettura dei files idml").create(
                        IDML_LIB_LICENSE_OPTION);
        Option configFileOption = OptionBuilder.withArgName("config_file").hasArg().withDescription("file di configurazione per licenza idml lib e percorso documenti").create(CONFIG_FILE_PATH_OPTION);
        Option rootPeOption =
                OptionBuilder.withArgName("root_pe_folder").hasArg().withDescription(
                        "percorso cartella principale EKR-PE").create(ROOT_PE_OPTION);
        Option pdfSendUrlOption =
                OptionBuilder.withArgName("send_url").hasArg().withDescription(
                        "url a cui viene inviata la POST contenente il PDF").create(PDF_SEND_URL_OPTION);
        Option pdfPostExampleOption =
                OptionBuilder.withArgName("file_esempio").hasArg().withDescription(
                        "file contenente un vettore JSON che descrive il contenuto della chiamata POST da fare verso URL di invio PDF").create(PDF_POST_EXAMPLE_OPTION);
        Option emptyEscapeOption = OptionBuilder.withArgName("empty_escape").hasArg().withDescription("indica che stringa viene usata in Selector per indicare la stringa vuota").create(EMPTY_ESCAPE_OPTION);
        
        Option toDeleteFilesOption = OptionBuilder.withArgName("files_da_cancellare").hasArgs().withDescription("elenco files da cancellare separati da | ").withValueSeparator('|').create(TO_DELETE_FILES_OPTION);
        
        commandLineOptions.addOption(helpCommandLineOption);
        commandLineOptions.addOption(sapXmlFileCommandLineOption);
        commandLineOptions.addOption(templateBaseDirOption);
        commandLineOptions.addOption(idmlLibLicenseOption);
        commandLineOptions.addOption(configFileOption);
        commandLineOptions.addOption(rootPeOption);
        commandLineOptions.addOption(emptyEscapeOption);
        commandLineOptions.addOption(pdfSendUrlOption);
        commandLineOptions.addOption(pdfPostExampleOption);
        commandLineOptions.addOption(toDeleteFilesOption);
        File xmlFromSapFile = null;
        File templateBaseDir = null;
        CommandLineParser parser = new BasicParser();
        try
        {
            CommandLine commandLine = parser.parse(commandLineOptions, args);
             String env = System.getenv(ROOT_PE_ENV_VAR);
            String rootPe = DEFAULT_ROOT_PE;
            if (env != null)
            {
                rootPe = env;
            }
            if (commandLine.hasOption(ROOT_PE_OPTION))
            {
                rootPe=commandLine.getOptionValue(ROOT_PE_OPTION);
            }
            File rootPeFile = new File(rootPe);
            Path rootPePath = rootPeFile.toPath();
            Path settingsTeDir = rootPePath.resolve(TE_SETTINGS_SUBDIR);
            Path teConfigPath = settingsTeDir.resolve(TE_CONFIG_FILE);
            File teConfigFile = teConfigPath.toFile();
            if (commandLine.hasOption(CONFIG_FILE_PATH_OPTION))
            {
                String configFilePath =
                        commandLine.getOptionValue(CONFIG_FILE_PATH_OPTION);
                teConfigFile = new File(configFilePath);
            }
            Properties configProperties = new Properties();
            if (teConfigFile.exists())
            {
                FileInputStream teConfigStream = new FileInputStream(teConfigFile);
                configProperties.load(teConfigStream);
            }
            if (commandLine.hasOption(HELP_OPTION))
            {
                showHelp(commandLineOptions);
            }

            if (commandLine.hasOption(XML_FROM_SAP_OPTION))
            {
                String xmlFromSapOptionValue = commandLine.getOptionValue(
                        XML_FROM_SAP_OPTION);
                xmlFromSapFile = new File(xmlFromSapOptionValue);
            }
            
            java.util.List<String> toDeleteFiles = null;
            if (commandLine.hasOption(TO_DELETE_FILES_OPTION))
            {
               String[] toDeleteFilesArray = commandLine.getOptionValues(TO_DELETE_FILES_OPTION);
               toDeleteFiles = new ArrayList<>();
               toDeleteFiles.addAll(Arrays.asList(toDeleteFilesArray));
            }
            
            URL pdfSendUrl = null;
            String pdfSendUrlValue = configProperties.getProperty(PDF_SEND_URL_PROPERTY);
            if (commandLine.hasOption(PDF_SEND_URL_OPTION))
            {
                pdfSendUrlValue = commandLine.getOptionValue(PDF_SEND_URL_OPTION);
            }
            if (pdfSendUrlValue != null)
            {
                pdfSendUrl = new URL(pdfSendUrlValue);
            }

            //l'array e l'url possono venire da file esterno o da xml per esigenze specifiche
            JsonObject pdfPostExample = null;
            String pdfPostExampleValue = configProperties.getProperty(PDF_POST_EXAMPLE_PROPERTY);
            if (commandLine.hasOption(PDF_POST_EXAMPLE_OPTION))
            {
                pdfPostExampleValue = commandLine.getOptionValue(PDF_POST_EXAMPLE_OPTION);
            }
            if (pdfPostExampleValue != null)
            {
                Path pdfPostExamplePath = settingsTeDir.resolve(pdfPostExampleValue);
                File pdfPostExampleFile = pdfPostExamplePath.toFile();
                JsonParser jsonParser = new JsonParser();
                FileInputStream inputStream = new FileInputStream(pdfPostExampleFile);
                InputStreamReader reader = new InputStreamReader(inputStream);
                pdfPostExample = (JsonObject) jsonParser.parse(reader);
            }
            
            String templateBaseDirFromConfig = configProperties.getProperty(TEMPLATE_BASE_DIR_PROPERTY,DEFAULT_TEMPLATE_BASE_DIR);
            templateBaseDir = new File(templateBaseDirFromConfig);
            if (commandLine.hasOption(CARTELLA_TEMPLATE))
            {
                String cartellaTemplateOptionValue =
                        commandLine.getOptionValue(CARTELLA_TEMPLATE);
                templateBaseDir = new File(cartellaTemplateOptionValue);
            }

            String emptyEscape = configProperties.getProperty(EMPTY_ESCAPE_SEQUENCE_PROPERTY, DEFAULT_EMPTY_ESCAPE_SEQUENCE);
            if (commandLine.hasOption(EMPTY_ESCAPE_OPTION))
            {
                emptyEscape = commandLine.getOptionValue(EMPTY_ESCAPE_OPTION);
            }
            
            
            String idmlLibLicense = configProperties.getProperty(IDML_LIB_LICENSE_PROPERTY,IDML_LIB_LICENSE);

            if (commandLine.hasOption(IDML_LIB_LICENSE_OPTION))
            {
                idmlLibLicense = commandLine.getOptionValue(
                        IDML_LIB_LICENSE_OPTION);
            }


            if (!teConfigFile.exists())
            {
                //se non esiste il file, lo creo in modo da avere un file di default da modificare
                configProperties.setProperty(TEMPLATE_BASE_DIR_PROPERTY, templateBaseDir.getCanonicalPath());
                configProperties.setProperty(IDML_LIB_LICENSE_PROPERTY, idmlLibLicense);
                configProperties.setProperty(EMPTY_ESCAPE_SEQUENCE_PROPERTY, emptyEscape);
                if (pdfSendUrl != null)
                {
                    configProperties.setProperty(PDF_SEND_URL_PROPERTY, pdfSendUrl.toString());
                }
                if (pdfPostExampleValue != null)
                {
                    configProperties.setProperty(PDF_POST_EXAMPLE_PROPERTY, pdfPostExampleValue);
                }
                   //aggiungo qui i valori provenienti dalla properties embedded
                ResourceBundle res = ResourceBundle
						.getBundle("com.ekrpe.scheduler.application");
				Enumeration<String> embeddedKeys = res.getKeys();
				while (embeddedKeys.hasMoreElements())
				{
					String key = embeddedKeys.nextElement();
					String value = res.getString(key);
					configProperties.setProperty(key, value);
				}
                File settingsTeDirFile = settingsTeDir.toFile();
                settingsTeDirFile.mkdirs();//creo le eventusali cartelle mancanti
                FileOutputStream outputStream = new FileOutputStream(teConfigFile);
                configProperties.store(outputStream, "impostazioni EKR-TE");
            }
            ParsingSapXml parserEngine = new ParsingSapXml();
            //Idml.setLicense(IDML_LIB_LICENSE);

            parserEngine.parseSapFile(xmlFromSapFile, templateBaseDir,
                   idmlLibLicense,emptyEscape,pdfSendUrl,pdfPostExample,toDeleteFiles);//*/
            /*parserEngine.setDataSourceFilePath(xmlFromSapFile.getCanonicalPath());
            parserEngine.doRun();//*/
            java.util.List<LoggerVO> logs = parserEngine.getLogs();
            for (LoggerVO log : logs)
            {
                Logger.getLogger(PdfFromSelector.class.getName()).
                    log(Level.INFO,log.getLogDescription());
            }
            
        }catch (ParseException | DOMException | IllegalArgumentException | IOException | SelectorDocumentException | ParserConfigurationException | SAXException | URISyntaxException | IdmlLibException | DocumentException | NoSuchFieldException | IllegalAccessException ex)
        {
            Logger.getLogger(PdfFromSelector.class.getName()).
                    log(Level.SEVERE, null, ex);
        }catch (SapFileException ex)
        {
            if (ex instanceof SapFileNotChoosedException)
            {
                System.out.println("Non è stato impostato il file SAP");
                showHelp(commandLineOptions);
            } else
            {
                System.out.println(ex.getMessage());
            }

            Logger.getLogger(PdfFromSelector.class.getName()).
                    log(Level.SEVERE, null, ex);
        } catch (Exception ex)
        {
            Logger.getLogger(PdfFromSelector.class.getName()).
                    log(Level.SEVERE, null, ex);
        }//*/
    }

    /**
     * mostra l'help
     * @param commandLineOptions le opzioni da riga di comando
     */
    public static void showHelp(
            Options commandLineOptions)
    {
        HelpFormatter helpFormatter = new HelpFormatter();
        helpFormatter.printHelp("PdfFromSelector", commandLineOptions);
    }

}
