package com.ekrpe.scheduler.runnable;

import java.util.ArrayList;
import java.util.List;

//import com.ekrpe.scheduler.criteria.ScheduledTaskVOCriteria;
import com.ekrpe.scheduler.model.LoggerVO;
import com.ekrpe.scheduler.model.ScheduledTaskVO;
//import com.ekrpe.scheduler.service.SchedulerManager;
import com.ekrpe.scheduler.util.ServiceRegistry;
 
public class TaskBase implements Runnable
{
    private Long jobId = null;
    private int progress=0;
    private int oldProgress;
    private String status=STATUS_PENDING;
    public static final String STATUS_PENDING="1";
    public static final String STATUS_RUNNING="2";
    public static final String STATUS_COMPLETED="3";
    public static final String STATUS_ERROR="4";
    
    private List<LoggerVO> logs= new ArrayList<LoggerVO>();
 
    private String dataSourceFilePath="";
    
    @Override
    public void run() {
        doRun();
    }
    


	public int getProgress() {
		return progress;
	}

	public void setProgress(int progress) {
				
		this.progress = progress;
		
		if (progress - oldProgress > 9){
			persist();
			oldProgress=this.progress;
		}		
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
		persist();
	}

	public List<LoggerVO> getLogs() {
		return logs;
	}

	public void addLog(LoggerVO log) {
		this.logs.add(log);
	}

	public String getDataSourceFilePath() {
		return dataSourceFilePath;
	}

	public void setDataSourceFilePath(String dataSourceFilePath) {
		this.dataSourceFilePath = dataSourceFilePath;
	}
	
	public void doRun(){
	    System.out.println("This method should never get called.");
	  }



	public Long getJobId() {
		return jobId;
	}



	public void setJobId(Long jobId) {
		this.jobId = jobId;
	}
	
	public void persist(){
		/*SchedulerManager schedulerManager =(SchedulerManager)ServiceRegistry.getBean("schedulerManager");
		ScheduledTaskVOCriteria criteria= new ScheduledTaskVOCriteria();
		criteria.setJobId(jobId);
		List<ScheduledTaskVO> list= schedulerManager.searchScheduleTaskVO(criteria).getScheduledTaskVOList();
		if (list!=null && list.size()==1){
			ScheduledTaskVO s= list.get(0);
			s.setJobStatus(status);
			s.setJobProgress(progress);
			schedulerManager.saveScheduledTaskVO(s);
		}//*/
		
	}
}