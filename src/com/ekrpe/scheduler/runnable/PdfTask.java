package com.ekrpe.scheduler.runnable;

import com.ekrpe.scheduler.model.LoggerVO;
 
public class PdfTask extends TaskBase
{
   
 
    
    public void doRun() {
    	setStatus(STATUS_RUNNING);
    	addLog(new LoggerVO("1", "Starting processing of SAP file " + getDataSourceFilePath()));
    	for (int i=0; i <=100; i++){
	        try {
	        	// Put your pdf generation code here..
	        	// This sleep is just to simulate the pdf processing, you can comment it..
	            Thread.sleep(200);
	            setProgress(i);
	        } catch (InterruptedException e) {
	            e.printStackTrace();
	        }
    	}
    	setStatus(STATUS_COMPLETED);
    	addLog(new LoggerVO("2", "Finished"));
    	
       
    }
}