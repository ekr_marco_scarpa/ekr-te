package com.ekrpe.scheduler.model;

import java.io.Serializable;
import java.util.Date;


public class ScheduledTaskVO implements Serializable{

	private Long jobId;
	private String mandante;
	private String jobIdOrigin;
	private String jobInputFile;
	private String jobOutputFile;
	private String jobStatus;
	private String jobStatusSescription;
	private int jobProgress;
	private Date submitTimestamp;
	private Date lastUpdateTimestamp;
	private String submittedByUsername;
	public Long getJobId() {
		return jobId;
	}
	public void setJobId(Long jobId) {
		this.jobId = jobId;
	}
	public String getMandante() {
		return mandante;
	}
	public void setMandante(String mandante) {
		this.mandante = mandante;
	}
	public String getJobIdOrigin() {
		return jobIdOrigin;
	}
	public void setJobIdOrigin(String jobIdOrigin) {
		this.jobIdOrigin = jobIdOrigin;
	}
	public String getJobInputFile() {
		return jobInputFile;
	}
	public void setJobInputFile(String jobInputFile) {
		this.jobInputFile = jobInputFile;
	}
	public String getJobOutputFile() {
		return jobOutputFile;
	}
	public void setJobOutputFile(String jobOutputFile) {
		this.jobOutputFile = jobOutputFile;
	}
	public String getJobStatus() {
		return jobStatus;
	}
	public void setJobStatus(String jobStatus) {
		this.jobStatus = jobStatus;
	}
	public String getJobStatusSescription() {
		return jobStatusSescription;
	}
	public void setJobStatusSescription(String jobStatusSescription) {
		this.jobStatusSescription = jobStatusSescription;
	}
	public int getJobProgress() {
		return jobProgress;
	}
	public void setJobProgress(int jobProgress) {
		this.jobProgress = jobProgress;
	}
	public Date getSubmitTimestamp() {
		return submitTimestamp;
	}
	public void setSubmitTimestamp(Date submitTimestamp) {
		this.submitTimestamp = submitTimestamp;
	}
	public Date getLastUpdateTimestamp() {
		return lastUpdateTimestamp;
	}
	public void setLastUpdateTimestamp(Date lastUpdateTimestamp) {
		this.lastUpdateTimestamp = lastUpdateTimestamp;
	}
	public String getSubmittedByUsername() {
		return submittedByUsername;
	}
	public void setSubmittedByUsername(String submittedByUsername) {
		this.submittedByUsername = submittedByUsername;
	}
	
	
	
	
}
