package com.ekrpe.scheduler.util;

public class Constants {

	public static final String FILE_PROCESSING_DIR_INBOX=Utilities.getSetting("FILE_PROCESSING_DIR_INBOX");
	public static final String FILE_PROCESSING_DIR_QUEUE=Utilities.getSetting("FILE_PROCESSING_DIR_QUEUE");
}
