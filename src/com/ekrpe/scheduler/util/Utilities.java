package com.ekrpe.scheduler.util;

import java.util.ResourceBundle;

import org.apache.log4j.Logger;
import org.springframework.web.context.WebApplicationContext;

public class Utilities {

	private static final Logger logger = Logger.getLogger(Utilities.class);
	
	public static String getSetting(String key){

	    String value="";
	    try{
	      ResourceBundle res = ResourceBundle.getBundle("com.ekrpe.scheduler.application");
	      value = res.getString(key);
	    }
	    catch(Exception e)
	    {
	    	logger.error(e.getMessage(), e);
	    }
	    return value;
	  }
	
	
}
